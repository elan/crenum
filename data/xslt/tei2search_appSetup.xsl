<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT TEI to Json : index for MiniSearch
* @author AnneGF@CNRS
* @date : 2024
* TODO : all books
* XML : Livre I.xml (à terme à changer donc...)
* Output : static/js/auto_app_minisearch.js

*/
-->

<!DOCTYPE xsl [
    <!ENTITY times "&#215;">
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:param name="data_json_file">
        <xsl:text>static/json/auto_index.json</xsl:text>
    </xsl:param>

    <xsl:param name="books" select="collection('../xml/?select=Livre_*.xml')"/>

    <xsl:param name="output">
        <xsl:text>../../static/js/auto_app_minisearch.js</xsl:text>
    </xsl:param>

    <xsl:variable name="br">
        <xsl:text>
</xsl:text>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:variable name="all_witness_id"
            select="distinct-values($books//@wit/tokenize(replace(replace(normalize-space(.), '&gt;', '_'),'#',''), ' '))"/>
        
        <xsl:variable name="witnesses_as_list">
            <xsl:for-each select="$all_witness_id">
                <xsl:text>'</xsl:text>
                <xsl:value-of select="."/>
                <xsl:text>'</xsl:text>
                <xsl:if test="position() &lt; last()">
                    <xsl:text>, </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <xsl:result-document href="{$output}">
            <xsl:text>// Setup data</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>let $searchable_fields = ['lem',</xsl:text>
            <xsl:value-of select="$witnesses_as_list"/>
            <xsl:text>];</xsl:text>
            <xsl:value-of select="$br"/>

            <xsl:text>let $showable_fields = ['type', 'book', 'chapter', 'n', 'lem',</xsl:text>
            <xsl:value-of select="$witnesses_as_list"/>
            <xsl:text>];</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:value-of select="$br"/>

            <xsl:text>// Setup MiniSearch</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>let miniSearch = new MiniSearch({</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>fields: $searchable_fields, // fields to index for full-text search</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>storeFields: $showable_fields // fields to return with search results</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>})</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:value-of select="$br"/>

            <xsl:text>let $data_json = '</xsl:text>
            <xsl:value-of select="$data_json_file"/>
            <xsl:text>';</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:value-of select="$br"/>

            <xsl:text disable-output-escaping="yes">const renderSearchResults = (results, searchOptions = getSearchOptions()) => {</xsl:text>
            <xsl:value-of select="$br"/>

            <xsl:text disable-output-escaping="yes">    //JSON.stringify(results);</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    const books = new Set(results.map(a => a.book));</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    let $result;</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    $resultsList.innerHTML = "";</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    $resultsList.innerHTML += "&lt;i class='small'>"+results.length+" résultat(s) trouvé(s)&lt;/i>";</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    for (let b of books){</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">        $resultsList.innerHTML += "&lt;h4>Livre "+b+"&lt;/h4>\n";</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">        results_by_book = results.filter((result) => result.book == b); //filter by book</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">        results_by_book.sort((a,b) => { //sort by verse number</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">          return (a.n - b.n);</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">        });</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:value-of select="$br"/>

            <xsl:text>    $resultsList.innerHTML += results_by_book.map(({ type, book, chapter, n, lem,</xsl:text>
            <xsl:variable name="simple_quote">
                <xsl:text>'</xsl:text>
            </xsl:variable>
            <xsl:value-of select="replace($witnesses_as_list, $simple_quote, '')"/>
            <xsl:text disable-output-escaping="yes"> }) => {</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    let $chap = `${chapter}`.substring(`${chapter}`.lastIndexOf("_")+1);</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    if ( `${type}` === "verse"){</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">      $result='&lt;h5 xmlns="http://www.w3.org/1999/xhtml" class="mt-3">Vers &lt;a href="';</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">      $result+=`/book${book}/${chapter}#${n}">${n}&lt;/a> (chapitre `</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">      $result+=$chap;</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">      $result+=`)&lt;/h5>`;</xsl:text>
            <xsl:text disable-output-escaping="yes">    }</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    if ( `${type}` === "page_header"){</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">      $result='&lt;h5 xmlns="http://www.w3.org/1999/xhtml" class="mt-3">Titre courant du folio &lt;a href="';</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">      $result+=`/book${book}/${chapter}/`+`${n.replace('f','p')}`+`/image-left_linear-right">${n}&lt;/a> (chapitre `</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">      $result+=$chap;</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">      $result+=`)&lt;/h5>`;</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text disable-output-escaping="yes">    }</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>$result+=`</xsl:text>
            <dl>
                <dt>BnFfr2817 :</dt>
                <dd>${lem}</dd>
                <xsl:text>`</xsl:text>
                <xsl:for-each select="$all_witness_id">
                    <xsl:value-of select="$br"/>
                    <xsl:text> if (! (`${</xsl:text>
                    <xsl:value-of select="replace(., '#', '')"/>
                    <xsl:text disable-output-escaping="yes">}` === "undefined") &amp;&amp; ! (searchOptions.fields.indexOf('</xsl:text>
                    <xsl:value-of select="replace(., '#', '')"/>
                    <xsl:text>') == -1)) {</xsl:text>
                    <xsl:value-of select="$br"/>
                    <xsl:text>   $result += `</xsl:text>
                    <dt><xsl:value-of select="replace(., '#', '')"/> : </dt>
                    <dd>
                        <xsl:text>${</xsl:text>
                        <xsl:value-of select="replace(., '#', '')"/>
                        <xsl:text>}</xsl:text>
                    </dd>
                    <xsl:value-of select="$br"/>
                    <xsl:text> `}</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="$br"/>
                <xsl:text> $result += `</xsl:text>
            </dl>
            <xsl:text>`</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>    return $result</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>}).join('\n')</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>}</xsl:text>
            <xsl:value-of select="$br"/>

            <xsl:text disable-output-escaping="yes">
        if (results.length > 0) {
            $app.classList.add('hasResults')
        } else {
            $app.classList.remove('hasResults')
        }
      }
      
      const getSuggestions = (query) => {
      return miniSearch.autoSuggest(query, { boost: { source_author: 3, fragment: 6} })
        .filter(({ suggestion, score }, _, [first]) => score > first.score / 4)
        .slice(0, 5)
      }
        </xsl:text>
        </xsl:result-document>
    </xsl:template>
</xsl:stylesheet>
