<?xml version="1.0" encoding="UTF-8"?><!--
/**

* XSLT for transformation of TEI to JSON : list of reigns
* @author ThéoRoulet@UGA & RachelGaubil@UGA
* @date : 2023
*/
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">


    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>
    <!-- Ignore les éléments ne contenant que des espaces et les tabulations-->
    <xsl:output method="text" encoding="UTF-8"/>


    <xsl:key name="k1"
        match="tei:relation[@name = 'precede']/@active | tei:relation[@name = 'precede']/@passive"
        use="."/>


    <xsl:template match="/">


        <xsl:text>[</xsl:text>
        
        <xsl:for-each
            select="(//tei:relation[@name = 'precede']/@active | //tei:relation[@name = 'precede']/@passive)[generate-id() = generate-id(key('k1', .)[1])]">

            <xsl:variable name="last"><xsl:value-of select="position()=last()"/></xsl:variable>
            
            <xsl:variable name="kingId" select="substring-after(., '#')"/>
            <xsl:variable name="kingNb"><xsl:value-of select="position()"/></xsl:variable>

            <!-- Si on a des dates de règne -->
            <xsl:for-each select="//tei:person[@xml:id = $kingId]/tei:occupation[@from and @to]">

                <!-- Exclure les titres autres (Duc, maire du palais ?)-->
                <xsl:if test="substring(., 1, 3) = 'Roi'">
                    
                    <xsl:text>{"</xsl:text>
                    
                    <!-- Id -->
                    <xsl:text>id":"</xsl:text>
                    <xsl:value-of select="concat($kingNb,'-',position())"/>
                    <xsl:text>","</xsl:text>
                    
                    <!-- Occupation -->
                    <xsl:text>title":"</xsl:text>
                    <xsl:value-of select="normalize-space(text())"/>
                    <xsl:text>","</xsl:text>

                    <!-- Name -->
                    <xsl:text>name":"</xsl:text>
                    <xsl:value-of
                        select="normalize-space(//tei:person[@xml:id = $kingId]/tei:persName[@type = 'normalise']/text())"/>
                    <xsl:text>","</xsl:text>

                    <!-- Reign start -->
                    <xsl:text>start":"</xsl:text>
                    <xsl:variable name="start"><xsl:value-of select="@from"/></xsl:variable>
                    <xsl:choose>
                        <xsl:when test="not(contains($start,'-'))">
                            <xsl:value-of select="concat($start,'-01-01')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$start"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>","</xsl:text>

                    <!-- Reign end -->
                    <xsl:text>end":"</xsl:text>
                    <xsl:variable name="end"><xsl:value-of select="@to"/></xsl:variable>
                    <xsl:choose>
                        <xsl:when test="not(contains($end,'-'))">
                            <xsl:value-of select="concat($end,'-01-01')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$end"/>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:text>"}</xsl:text>
                    
                    <xsl:if test="not(position() = last() and $last = 'true')">
                        <xsl:text>,</xsl:text>
                    </xsl:if>
                </xsl:if>

            </xsl:for-each>

        </xsl:for-each>
        
        <xsl:text>]</xsl:text>
        
    </xsl:template>







</xsl:stylesheet>
