<?xml version="1.0" encoding="UTF-8"?><!--
/**

* XSLT for transformation of TEI to HTML : table of contents
* @author RachelGaubil@UGA
* @date : 2023-205
*/
-->

<!DOCTYPE tei2tableOfContents [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">
    <xsl:import href="tei2chapter.xsl"/>
    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!--Enlève les espaces superflus-->
    <xsl:strip-space elements="*"/>

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">
        <xsl:variable name="tei" select="current()"/>

        <xsl:variable name="output1">
            <xsl:text>../../templates/htm/chapters_list.htm</xsl:text>
        </xsl:variable>
        
        <!-- generate chapters list (in chapters view -->
        <xsl:result-document method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"
            href="{$output1}">

            <xsl:for-each select="tei:TEI/tei:text/tei:body//tei:div[@type = 'book']">
                <xsl:variable name="book" select="@n"/>

                <xsl:text>{% macro book</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>() %}</xsl:text>
                <div class="col-8 text-center">

                    <xsl:for-each select="./tei:div">
                        <xsl:variable name="chap" as="xs:integer">
                            <xsl:value-of select="position()"/>
                        </xsl:variable>
                        <xsl:variable name="chap_link">
                            <xsl:text>/book</xsl:text>
                            <xsl:value-of select="$book"/>
                            <xsl:text>/</xsl:text>
                            <!-- ne pas compter le prologue en prose du livre I-->
                            <xsl:choose>
                                <xsl:when test="$book = 1">
                                    <xsl:value-of select="$chap - 1"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$chap"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text>_</xsl:text>
                            <xsl:value-of select="@type"/>
                            <xsl:if test="@n">
                                <xsl:text>_</xsl:text>
                                <xsl:value-of select="@n"/>
                            </xsl:if>
                        </xsl:variable>

                        <xsl:if test="@type != 'prologueProse'">
                            <xsl:call-template name="generate_chapters_list">
                                <xsl:with-param name="link" select="$chap_link"/>
                            </xsl:call-template>
                        </xsl:if>

                    </xsl:for-each>
                </div>
                <xsl:text>
                    {% endmacro %}
                </xsl:text>
            </xsl:for-each>
        </xsl:result-document>


        <xsl:for-each select="tei:TEI/tei:text/tei:body//tei:div[@type = 'book']">
            <xsl:sort select="@n" data-type="number"/>
            <xsl:variable name="book" select="@n"/>
            <xsl:variable name="output2">
                <xsl:text>../../templates/htm/table_of_contents_</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>.htm</xsl:text>
            </xsl:variable>
            
            <!-- Generate chapters list (in menu view) -->
            <xsl:result-document method="html" indent="yes" encoding="UTF-8"
                omit-xml-declaration="yes" href="{$output2}">

                <a class="dropdown-item summary" role="button">
                    <xsl:attribute name="href">
                        <xsl:text>/summary_book</xsl:text>
                        <xsl:value-of select="$book"/>
                    </xsl:attribute>
                    <xsl:text>Sommaire</xsl:text>
                </a>

                <xsl:for-each select="./tei:div">
                    <xsl:variable name="chap" as="xs:integer">
                        <xsl:value-of select="position()"/>
                    </xsl:variable>
                    <xsl:variable name="chap_link">
                        <xsl:text>/book</xsl:text>
                        <xsl:value-of select="$book"/>
                        <xsl:text>/</xsl:text>
                        <!-- ne pas compter le prologue en prose du livre I-->
                        <xsl:choose>
                            <xsl:when test="$book = 1">
                                <xsl:value-of select="$chap - 1"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$chap"/>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:text>_</xsl:text>
                        <xsl:value-of select="@type"/>
                        <xsl:if test="@n">
                            <xsl:text>_</xsl:text>
                            <xsl:value-of select="@n"/>
                        </xsl:if>
                    </xsl:variable>

                    <xsl:if test="@type != 'prologueProse'">

                        <li id="{concat('book',$book,'chapter',$chap)}">
                            <xsl:call-template name="generate_chapters_tableOfCont">
                                <xsl:with-param name="book" select="$book"/>
                                <xsl:with-param name="chap" select="$chap"/>
                                <xsl:with-param name="link" select="$chap_link"/>
                            </xsl:call-template>
                        </li>
                    </xsl:if>

                </xsl:for-each>

            </xsl:result-document>


            <!-- Generate table of content (in summary view) -->
            <xsl:variable name="output3">
                <xsl:text>../../templates/htm/summary_book</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>.htm</xsl:text>
            </xsl:variable>

            <xsl:result-document method="html" indent="yes" encoding="UTF-8"
                omit-xml-declaration="yes" href="{$output3}">

                <ul class="summary">

                    <xsl:for-each select="./tei:div">
                        <xsl:variable name="chap" as="xs:integer">
                            <xsl:value-of select="position()"/>
                        </xsl:variable>
                        <xsl:variable name="chap_link">
                            <xsl:text>/book</xsl:text>
                            <xsl:value-of select="$book"/>
                            <xsl:text>/</xsl:text>
                            <!-- ne pas compter le prologue en prose du livre I-->
                            <xsl:choose>
                                <xsl:when test="$book = 1">
                                    <xsl:value-of select="$chap - 1"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$chap"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text>_</xsl:text>
                            <xsl:value-of select="@type"/>
                            <xsl:if test="@n">
                                <xsl:text>_</xsl:text>
                                <xsl:value-of select="@n"/>
                            </xsl:if>
                        </xsl:variable>
                        <xsl:variable name="chap_num">
                            <xsl:choose>
                                <xsl:when test="@type = 'prologueVerse'">
                                    <xsl:text>Prologue en vers</xsl:text>
                                </xsl:when>
                                <xsl:when test="@type = 'chap'">
                                    <xsl:text>Chapitre </xsl:text>
                                    <xsl:value-of select="@n"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:variable>


                        <xsl:if test="@type != 'prologueProse'">
                            <h5 class="btn-icons">
                                <a data-bs-toggle="collapse" role="button" aria-expanded="false"
                                    aria-controls="collapseExample">
                                    <xsl:attribute name="href">
                                        <xsl:text>#</xsl:text>
                                        <xsl:value-of select="@type"/>
                                        <xsl:value-of select="@n"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="$chap_num"/>
                                    <xsl:text> </xsl:text>
                                    <i class="fa-solid fa-angle-down"/>
                                </a>
                            </h5>

                            <p>
                                <xsl:if test="./tei:head">
                                    <xsl:for-each select="./tei:head//tei:l">
                                        <span class="verses">
                                            <xsl:apply-templates select="." mode="linear"/>
                                        </span>
                                    </xsl:for-each>
                                </xsl:if>
                            </p>

                            <ul class="summary collapse">
                                <xsl:attribute name="id">
                                    <xsl:value-of select="@type"/>
                                    <xsl:value-of select="@n"/>
                                </xsl:attribute>
                                <xsl:attribute name="aria-labelledby">
                                    <xsl:text>headingBook</xsl:text>
                                    <xsl:value-of select="$book"/>
                                    <xsl:text>Chap</xsl:text>
                                    <xsl:value-of select="$chap"/>
                                </xsl:attribute>

                                <xsl:choose>

                                    <!-- Prologues en vers sans titre courant -->
                                    <xsl:when
                                        test="@type = 'prologueVerse' and not(.//tei:fw[@type = 'header'])">
                                        <xsl:variable name="title_link">
                                            <xsl:value-of select="$chap_link"/>
                                            <xsl:text>/p</xsl:text>
                                            <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                            <xsl:value-of
                                                select="substring-before(substring-after(following::tei:pb[1]/@facs, '/f'), '.item')"
                                            />
                                        </xsl:variable>
                                        <xsl:call-template name="generate_titles_tableOfCont">
                                            <xsl:with-param name="value">
                                                <xsl:text>Prologue en vers</xsl:text>
                                            </xsl:with-param>
                                            <xsl:with-param name="title">
                                                <xsl:text>false</xsl:text>
                                            </xsl:with-param>
                                            <xsl:with-param name="link" select="$title_link"/>
                                        </xsl:call-template>
                                    </xsl:when>

                                    <xsl:otherwise>

                                        <!-- Cas des chapitres en vers sans titre courant -> pas besoin si on parcours les folios ?-->
                                        <xsl:if
                                            test=".//tei:head[@type = 'introVerse'] and .//tei:head[@type = 'introVerse' and not(preceding-sibling::tei:fw[@type = 'header'])]">
                                            <xsl:for-each
                                                select=".//tei:head[@type = 'introVerse']//tei:l[1]">
                                                <xsl:variable name="title_link">
                                                  <xsl:value-of select="$chap_link"/>
                                                  <xsl:text>/p</xsl:text>
                                                  <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                                  <xsl:value-of
                                                  select="substring-before(substring-after(preceding::tei:pb[1]/@facs, '/f'), '.item')"
                                                  />
                                                </xsl:variable>
                                                <xsl:call-template
                                                  name="generate_titles_tableOfCont">
                                                  <xsl:with-param name="value">
                                                  <xsl:text/>
                                                  </xsl:with-param>
                                                  <xsl:with-param name="title">
                                                  <xsl:text>false</xsl:text>
                                                  </xsl:with-param>
                                                  <xsl:with-param name="link" select="$title_link"/>
                                                </xsl:call-template>
                                            </xsl:for-each>
                                        </xsl:if>

                                        <xsl:for-each select=".//tei:fw[@type = 'folio']">
                                            <span class="ok"/>
                                            <xsl:choose>

                                                <!-- empty folio -->
                                                <xsl:when
                                                  test="following-sibling::*[1][self::tei:div] or following-sibling::*[1][self::tei:pb]">
                                                  <xsl:variable name="title_link">
                                                  <xsl:value-of select="$chap_link"/>
                                                  <xsl:text>/p</xsl:text>
                                                  <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                                  <xsl:value-of
                                                  select="substring-before(substring-after(preceding::tei:pb[1]/@facs, '/f'), '.item')"
                                                  />
                                                  </xsl:variable>
                                                  <xsl:call-template
                                                  name="generate_titles_tableOfCont">
                                                  <xsl:with-param name="value">
                                                  <xsl:text>miniature</xsl:text>
                                                  </xsl:with-param>
                                                  <xsl:with-param name="title">
                                                  <xsl:text>false</xsl:text>
                                                  </xsl:with-param>
                                                  <xsl:with-param name="link" select="$title_link"/>
                                                  </xsl:call-template>
                                                </xsl:when>

                                                <!-- Chapitres avec titres courants -->
                                                <xsl:when
                                                  test="following-sibling::tei:fw[1][@type = 'header']">
                                                  <xsl:for-each
                                                  select="following-sibling::tei:fw[1][@type = 'header']">
                                                  <xsl:variable name="title_link">
                                                  <xsl:value-of select="$chap_link"/>
                                                  <xsl:text>/p</xsl:text>
                                                  <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                                  <xsl:value-of
                                                  select="substring-before(substring-after(preceding::tei:pb[1]/@facs, '/f'), '.item')"
                                                  />
                                                  </xsl:variable>
                                                  <xsl:call-template
                                                  name="generate_titles_tableOfCont">
                                                  <xsl:with-param name="value">
                                                  <xsl:text/>
                                                  </xsl:with-param>
                                                  <xsl:with-param name="title">
                                                  <xsl:text>true</xsl:text>
                                                  </xsl:with-param>
                                                  <xsl:with-param name="link" select="$title_link"/>
                                                  </xsl:call-template>
                                                  </xsl:for-each>
                                                </xsl:when>

                                                <!-- Chapitres sans titres courants -->
                                                <xsl:otherwise>
                                                  <xsl:variable name="title_link">
                                                  <xsl:value-of select="$chap_link"/>
                                                  <xsl:text>/p</xsl:text>
                                                  <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                                  <xsl:value-of
                                                  select="substring-before(substring-after(preceding::tei:pb[1]/@facs, '/f'), '.item')"
                                                  />
                                                  </xsl:variable>
                                                  <xsl:call-template
                                                  name="generate_titles_tableOfCont">
                                                  <xsl:with-param name="value">
                                                  <xsl:text/>
                                                  </xsl:with-param>
                                                  <xsl:with-param name="title">
                                                  <xsl:text>false</xsl:text>
                                                  </xsl:with-param>
                                                  <xsl:with-param name="link" select="$title_link"/>
                                                  </xsl:call-template>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:for-each>
                                    </xsl:otherwise>

                                </xsl:choose>
                            </ul>
                        </xsl:if>

                    </xsl:for-each>
                </ul>
            </xsl:result-document>

        </xsl:for-each>
    </xsl:template>


    <xsl:template name="generate_chapters_list">
        <xsl:param name="link" required="yes"/>

        <a class="btn-link">
            <xsl:attribute name="href">
                <xsl:value-of select="$link"/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="@type = 'chap'">
                    <xsl:text>Chapitre </xsl:text>
                    <xsl:value-of select="@n"/>
                </xsl:when>
                <xsl:when test="@type = 'prologueVerse'">
                    <xsl:text>Prologue en vers</xsl:text>
                </xsl:when>
            </xsl:choose>
        </a>
        <xsl:if test="not(position() = last())">
            <xsl:text> | </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template name="generate_chapters_tableOfCont">
        <xsl:param name="book" required="yes"/>
        <xsl:param name="chap" required="yes"/>
        <xsl:param name="link" required="yes"/>


        <a class="dropdown-item btn-summary" role="button">
            <xsl:attribute name="id">
                <xsl:text>headingBook</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>Chap</xsl:text>
                <xsl:value-of select="$chap"/>
            </xsl:attribute>

            <xsl:attribute name="href">
                <xsl:value-of select="$link"/>
                <xsl:text>/p</xsl:text>

                <xsl:variable name="gallicaUrl">
                    <xsl:choose>
                        <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant -->
                        <xsl:when test="child::*[1]/name() = 'pb'">
                            <xsl:value-of select="./tei:pb/@facs"/>
                        </xsl:when>
                        <!-- récupération du numéro de page depuis l'url gallica dans le pb précédent -->
                        <xsl:otherwise>
                            <xsl:value-of select="preceding::tei:pb[1]/@facs"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:value-of select="substring-before(substring-after($gallicaUrl, '/f'), '.item')"
                />
            </xsl:attribute>
            <span class="chapterType">
                <xsl:choose>
                    <xsl:when test="@type = 'prologueVerse'">
                        <xsl:text>Prologue en vers</xsl:text>
                    </xsl:when>
                    <xsl:when test="@type = 'chap'">
                        <xsl:text>Chapitre </xsl:text>
                        <xsl:value-of select="@n"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@type"/>
                    </xsl:otherwise>
                </xsl:choose>
            </span>

            <span class="versesRange">
                <xsl:text>[v. </xsl:text>
                <xsl:value-of select="descendant::tei:l[1]/@n"/>
                <xsl:text> - </xsl:text>
                <!-- if preceding verse doesnt have @n or if its a collation (not a numeric @n), search the preceding verse -->
                <xsl:choose>
                    <xsl:when test="descendant::tei:l[last()]/not(@n) or translate(descendant::tei:l[last()]/@n, '0123456789', '') != ''">
                        <xsl:call-template name="verseSearching">
                            <xsl:with-param name="counter" select="count(1)"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="descendant::tei:l[last()]/@n"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>]</xsl:text>
            </span>
        </a>
    </xsl:template>

    <xsl:template name="verseSearching">
        <xsl:param name="counter" required="yes"/>
        <xsl:choose>
            <!-- if preceding verse doesnt have @n or if its a collation (not a numeric @n), search the preceding verse again -->
            <xsl:when test="descendant::tei:l[last()]/preceding::tei:l[$counter]/not(@n) or translate(descendant::tei:l[last()]/preceding::tei:l[$counter]/@n, '0123456789', '') != ''">
                <xsl:call-template name="verseSearching">
                    <xsl:with-param name="counter" select="$counter + 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="descendant::tei:l[last()]/preceding::tei:l[$counter]/@n"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="generate_titles_tableOfCont">
        <xsl:param name="value" required="yes"/>
        <xsl:param name="title" required="yes"/>
        <xsl:param name="link" required="yes"/>

        <xsl:variable name="currVerseNum">
            <xsl:choose>
                <xsl:when test="./name() = 'l'">
                    <xsl:value-of select="@n"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="following::tei:l[1]/@n"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <li>
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="$link"/>
                    <xsl:if test="$value != 'miniature'">
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="$currVerseNum"/>
                    </xsl:if>
                </xsl:attribute>

                <span class="currentTitle">

                    <xsl:choose>
                        <xsl:when test="$value != ''">
                            <xsl:text>[</xsl:text>
                            <xsl:value-of select="$value"/>
                            <xsl:text>]</xsl:text>
                        </xsl:when>

                        <xsl:otherwise>
                            <xsl:choose>
                                <!-- if header : verify that current header is the good one -->
                                <xsl:when test="$title = 'true'">
                                    <xsl:choose>
                                        <xsl:when test="@corresp">
                                            <!-- 1 corresp => header = preceding page header -->
                                            <xsl:if test="not(contains(@corresp, ' '))">
                                                <xsl:text>[</xsl:text>
                                                <xsl:apply-templates
                                                  select="preceding::tei:fw[@type = 'header'][1]"
                                                  mode="linear"/>
                                                <xsl:text>]</xsl:text>
                                            </xsl:if>
                                        </xsl:when>
                                        <!-- 2 corresp => header = current header -->
                                        <!-- header = current header -->
                                        <xsl:otherwise>
                                            <xsl:apply-templates select="current()" mode="linear"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:when>
                                <!-- if not header -->
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <!-- preceding header = 1 or 2 corresp => current header = preceding header -->
                                        <xsl:when
                                            test="preceding::tei:fw[@type = 'header'][1]/@corresp">
                                            <xsl:text>[</xsl:text>
                                            <xsl:apply-templates
                                                select="preceding::tei:fw[@type = 'header'][1]"
                                                mode="linear"/>
                                            <xsl:text>]</xsl:text>
                                        </xsl:when>
                                        <!-- preceding header = 0 corresp => current header = following header -->
                                        <xsl:otherwise>
                                            <xsl:choose>
                                                <!-- exist preceding header -->
                                                <xsl:when
                                                  test="count(preceding-sibling::tei:fw[@type = 'folio']) > 0">
                                                  <xsl:text>[</xsl:text>
                                                  <xsl:apply-templates
                                                  select="preceding::tei:fw[@type = 'header'][1]"
                                                  mode="linear"/>
                                                  <xsl:text>]</xsl:text>
                                                </xsl:when>
                                                <!-- not exist preceding header -->
                                                <xsl:otherwise>
                                                  <xsl:text>[</xsl:text>
                                                  <xsl:apply-templates
                                                  select="following::tei:fw[@type = 'header'][1]"
                                                  mode="linear"/>
                                                  <xsl:text>]</xsl:text>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>

                </span>

                <xsl:if test="$value != 'miniature'">
                    <span class="versesRange">
                        <xsl:text>v. </xsl:text>
                        <xsl:value-of select="$currVerseNum"/>
                    </span>
                </xsl:if>

            </a>
        </li>
    </xsl:template>

    <!-- plus nécessaire si pas de découpage de 1° vers/ligne -->
    <xsl:template match="tei:lb" mode="linear">
        <span class="lb"/>
    </xsl:template>

    <xsl:template match="tei:app" mode="linear">
        <xsl:value-of select="./tei:lem"/>
    </xsl:template>

    <xsl:template match="tei:l" mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template
        match="tei:abbr | tei:expan | tei:geogName | tei:orgName | tei:placeName | tei:persName | tei:metamark | tei:fw"
        mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:rdg | tei:note" mode="linear"/>

</xsl:stylesheet>
