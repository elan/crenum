<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT TEI to Json : index for MiniSearch
* @author AnneGF@CNRS
* @date : 2024
* TODO : all books + prologue
* XML : Livre I.xml (à terme à changer donc...)
* Output : static/json/auto_index.json
*/
-->

<!DOCTYPE xsl [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="text" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:param name="books" select="collection('../xml/?select=Livre_*.xml')"/>

    <xsl:param name="output">
        <xsl:text>../../static/json/auto_index.json</xsl:text>
    </xsl:param>

    <xsl:variable name="br_pretty_print">
        <xsl:text>
</xsl:text>
    </xsl:variable>

    <xsl:variable name="br_minify">
        <xsl:text> </xsl:text>
    </xsl:variable>
    <xsl:variable name="br" select="$br_pretty_print"/>

    <xsl:strip-space elements="tei:choice"/>
    <xsl:strip-space elements="tei:l"/>
    <!--<xsl:preserve-space elements="*"/>-->
    <!--<xsl:preserve-space elements="tei:ab tei:bibl tei:note tei:title"/>-->

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">
        <xsl:variable name="tei" select="$books"/>

        <xsl:result-document href="{$output}">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:for-each select="$books">
                <xsl:sort select=".//tei:div[@type='book']/@n"/>
                <xsl:for-each select="tei:TEI/tei:text/tei:body//tei:div[@type = 'book']">
                    <xsl:sort select="@n" data-type="number"/>

                    <xsl:variable name="book" select="@n"/>

                    <xsl:for-each select=".//tei:l|.//tei:fw[@type='header']">
                        <xsl:variable name="l" select="." as="node()"/>
                        <xsl:variable name="l_count" select="position()"/>
                        <xsl:variable name="chapter">
                            <xsl:variable name="n_chap">
                                <xsl:number select="." count="tei:div" level="single"/>
                            </xsl:variable>
                            <xsl:value-of select="$n_chap - 1"/>
                            <xsl:text>_chap_</xsl:text>
                            <xsl:value-of select="./ancestor::tei:div[@type = 'chap']/@n"/>
                        </xsl:variable>
                        <xsl:text>  {</xsl:text>
                        <xsl:value-of select="$br"/>

                        <!-- Id -->
                        <xsl:text>    "id": "</xsl:text>
                        <xsl:value-of select="$book"/>
                        <xsl:value-of select="$l_count"/>
                        <xsl:text>",</xsl:text>
                        <xsl:value-of select="$br"/>
                        
                        <!-- Type -->
                        <xsl:variable name="type">
                            <xsl:choose>
                                <xsl:when test="name(.) = 'l'">
                                    <xsl:text>verse</xsl:text>
                                </xsl:when>
                                <xsl:when test="name(.) = 'fw'">
                                    <xsl:text>page_header</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>other</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:text>    "type": "</xsl:text>
                        <xsl:value-of select="$type"/>
                        <xsl:text>",</xsl:text>
                        <xsl:value-of select="$br"/>

                        <!-- Numero du livre -->
                        <xsl:text>    "book": "</xsl:text>
                        <xsl:value-of select="$book"/>
                        <xsl:text>",</xsl:text>
                        <xsl:value-of select="$br"/>

                        <!-- Numero du chapitre -->
                        <xsl:text>    "chapter": "</xsl:text>
                        <xsl:value-of select="$chapter"/>
                        <xsl:text>",</xsl:text>
                        <xsl:value-of select="$br"/>

                        <!-- Numero de vers (ou page pour un titre courant) -->
                        <xsl:variable name="l_num">
                            <xsl:choose>
                                <xsl:when test="$type = 'verse' and not(@n = '')">
                                    <xsl:value-of select="@n"/>
                                </xsl:when>
                                <xsl:when test="$type = 'page_header'">
                                    <xsl:value-of select="preceding-sibling::tei:pb[1]/replace(@facs,'.*/(.*).item','$1')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>N.A.</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:text>    "n": "</xsl:text>
                        <xsl:value-of select="$l_num"/>
                        <xsl:text>",</xsl:text>
                        <xsl:value-of select="$br"/>

                        <!-- Témoins -->
                        <xsl:for-each-group select=".//*/@wit/tokenize(., ' ')" group-by=".">
                            <xsl:variable name="witness " select="."/>
                            <xsl:if
                                test="exists($l//*[contains(@wit, $witness) and not(@type = 'irrelevant')])">
                                <xsl:variable name="lesson">
                                    <xsl:apply-templates select="$l" mode="witness">
                                        <xsl:with-param name="wit" select="$witness" tunnel="yes"/>
                                    </xsl:apply-templates>
                                </xsl:variable>

                                <!-- Témoin -->
                                <xsl:text>    "</xsl:text>
                                <xsl:value-of select="replace($witness, '#', '')"/>
                                <xsl:text>": "</xsl:text>
                                <xsl:value-of select="normalize-space($lesson)"/>
                                <xsl:text>",</xsl:text>
                                <xsl:value-of select="$br"/>
                            </xsl:if>
                            <!--
                    <xsl:if test="$l_num = '411'">
                        <xsl:message>411 - <xsl:value-of select="$witness"/></xsl:message>
                    </xsl:if>
                    <xsl:if test="$l_num = '412'">
                        <xsl:message terminate="yes"> Debug - Stops for specifiied $l_num</xsl:message>
                    </xsl:if>-->
                        </xsl:for-each-group>

                        <!-- Lemme -->
                        <xsl:variable name="lem">
                            <xsl:apply-templates mode="lem"/>
                        </xsl:variable>
                        <xsl:text>    "lem": "</xsl:text>
                        <xsl:value-of select="normalize-space($lem)"/>
                        <xsl:text>"</xsl:text>
                        <xsl:value-of select="$br"/>

                        <!-- Closing Json -->
                        <xsl:text>  }</xsl:text>
                        <xsl:if test="position() &lt; last()">
                            <xsl:text>,</xsl:text>
                        </xsl:if>
                        <xsl:value-of select="$br"/>
                    </xsl:for-each>
                </xsl:for-each>
                <xsl:if test="position() &lt; last()">
                    <xsl:text>,</xsl:text>
                    <xsl:value-of select="$br"/>
                </xsl:if>
            </xsl:for-each>
            <xsl:text>]</xsl:text>
            <xsl:value-of select="$br"/>
        </xsl:result-document>
    </xsl:template>

    <!-- Template for both lemma and witnesses versions -->
    <xsl:template match="
            tei:reg | tei:persName | tei:placeName | tei:orgName |
            tei:geogName | tei:pc | tei:date | tei:said | tei:subst | tei:add |
            tei:num | tei:foreign | tei:hi | tei:quote | tei:title | tei:unclear |
            tei:gap" mode="lem witness">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:choice" mode="lem witness">
        <xsl:apply-templates select="tei:reg" mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:del" mode="lem witness">
        <!-- del à vérifier : que renvoit-on comme résultat de recherche ?
        Pour le moment, <s></s> car mais barré... c'est déjà présent donc peut être renvoyé par la recherche...
        mais on pourrait aussi l'enlever en considérant la forme absents du témoin...
        cf. vers numéro 411 pour un exemple (rdg wit="#Aix419") -->
        <xsl:if test="not(. = '')">
            <xsl:text disable-output-escaping="yes">&lt;s></xsl:text>
            <xsl:apply-templates mode="#current"/>
            <xsl:text disable-output-escaping="yes">&lt;/s></xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:metamark | tei:orig | tei:note" mode="lem witness"/>


    <!-- Templates for lemma version -->
    <xsl:template match="tei:app" mode="lem">
        <xsl:apply-templates select="tei:lem" mode="lem"/>
    </xsl:template>

    <xsl:template match="tei:lem" mode="lem">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>


    <!-- Templates for a specific witness version -->
    <xsl:template match="tei:app" mode="witness">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:l" mode="witness">
        <xsl:param name="wit" tunnel="yes"/>
        <xsl:apply-templates mode="witness"/>
    </xsl:template>

    <xsl:template match="tei:lem | tei:rdg" mode="witness">
        <xsl:param name="wit" tunnel="yes"/>
        <xsl:if test="
                contains(@wit, $wit)
                or (
                not(following::*[contains(@wit, $wit)])
                and not(preceding::*[contains(@wit, $wit)])
                )">
            <!-- todo:  and not(.//contains(@wit, $wit)) -->
            <xsl:apply-templates mode="witness"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:rdgGrp" mode="witness">
        <xsl:param name="wit" tunnel="yes"/>
        <xsl:apply-templates mode="witness"/>
    </xsl:template>

    <xsl:template match="tei:rdgGrp[@type = 'irrelevant']" mode="witness"/>

    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*" mode="#all">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
        <xsl:message>
            <xsl:text>Élement non pris en charge par la XSLT : </xsl:text>
            <xsl:value-of select="name(.)"/>
            <xsl:copy/>
            <xsl:copy-of select="@*"/>
        </xsl:message>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
