<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation of facsimiles from XML to HTML
* @author RachelGaubil@UGA
* @date : 2024
*/
-->
<!DOCTYPE facsimiles [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:strip-space elements="*"/>

    <xsl:variable name="img_width" select="1600"/>

    <xsl:template match="/">
        <xsl:variable name="tei" select="current()"/>

        <xsl:for-each select="tei:TEI/tei:facsimile">
            <!--            <xsl:sort select="@corresp" data-type="text"/>-->
            <xsl:variable name="output">
                <xsl:text>../../templates/htm/paints/paints</xsl:text>
                <!--                <xsl:value-of select="$fileNum"/>-->
                <xsl:value-of select="position()"/>
                <xsl:text>.htm.j2</xsl:text>
            </xsl:variable>

            <xsl:variable name="facsimileRef" select="@corresp"/>

            <xsl:result-document method="html" indent="yes" encoding="UTF-8"
                omit-xml-declaration="yes" href="{$output}">
                <xsl:call-template name="create_block_toc">
                    <xsl:with-param name="tei" select="$tei" tunnel="yes"/>
                    <xsl:with-param name="facsimileRef" select="$facsimileRef" tunnel="yes"/>
                </xsl:call-template>
                <xsl:call-template name="create_block_description">
                    <xsl:with-param name="tei" select="$tei" tunnel="yes"/>
                    <xsl:with-param name="facsimileRef" select="$facsimileRef" tunnel="yes"/>
                </xsl:call-template>
                <xsl:call-template name="create_block_biblio">
                    <xsl:with-param name="tei" select="$tei" tunnel="yes"/>
                    <xsl:with-param name="facsimileRef" select="$facsimileRef" tunnel="yes"/>
                </xsl:call-template>
                <xsl:call-template name="create_block_img">
                    <xsl:with-param name="tei" select="$tei" tunnel="yes"/>
                    <xsl:with-param name="facsimileRef" select="$facsimileRef" tunnel="yes"/>
                </xsl:call-template>
                <xsl:call-template name="create_block_note">
                    <xsl:with-param name="tei" select="$tei" tunnel="yes"/>
                    <xsl:with-param name="facsimileRef" select="$facsimileRef" tunnel="yes"/>
                </xsl:call-template>
            </xsl:result-document>
            <xsl:message>
                <xsl:value-of select="$output"/>
            </xsl:message>

        </xsl:for-each>

    </xsl:template>

    <!-- TOC -->

    <xsl:template name="create_block_toc">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="facsimileRef" required="yes" tunnel="yes"/>
        <xsl:text>
{% macro toc(pos) %}
</xsl:text>
        <xsl:for-each select="$tei/tei:TEI/tei:facsimile[@corresp = $facsimileRef]/tei:surface">
            <xsl:sort data-type="number"
                select="substring-before(substring-after(@facs, '/f'), '.item')"/>
            <xsl:variable name="data-prefix"
                select="replace(@facs, '(.*)/ark:/([^/]+/).*', '$1/iiif/ark:/$2')"/>
            <xsl:variable name="data-suffix" select="concat('/full/', $img_width, ',/0/color.jpg')"/>
            <xsl:variable name="data-ident" select="replace(@facs, '.*/ark:/[^/]+/(.*).item', '$1')"/>
            <xsl:variable name="id" select="replace(@facs, '.*/(.*).item', '$1')"/>
            <span class="img-thumbnail-container">
                <img class="img-thumbnail" src="{$data-prefix}{$data-ident}/full/,80/0/color.jpg"
                    data-img="{$data-prefix}{$data-ident}{$data-suffix}"
                    data-credit="Source : BnF / Gallica">
                    <xsl:attribute name="id">
                        <xsl:value-of select="$id"/>
                    </xsl:attribute>
                </img>
                <span>
                    <xsl:value-of select="@n"/>
                </span>
            </span>
        </xsl:for-each>
        <hr/>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>

    <!-- Img -->
    <xsl:template name="create_block_img">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="facsimileRef" required="yes" tunnel="yes"/>
        <xsl:text>
{% macro img(pos) %}
</xsl:text>
        <div class="osd-pane tab-pane fade" role="tabpanel" tabindex="0">
            <xsl:attribute name="id">
                <xsl:text>image-{{pos}}</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="aria-labelledby">
                <xsl:text>image-{{pos}}-tab</xsl:text>
            </xsl:attribute>
            <div class="osd-pane-content">
                <xsl:attribute name="id">
                    <xsl:text>osd-pane-content-{{pos}}</xsl:text>
                </xsl:attribute>
                <div class="osd-btn text-center">
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-zoom-in-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-search-plus"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-zoom-out-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-search-minus"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-home-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-home"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-full-page-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-arrows-alt"/>
                    </span>
                    <!--<span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-left-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-undo"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-right-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-redo-alt"/>
                    </span>-->
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-zones-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa-solid fa-draw-polygon"></i>
                    </span>
                </div>
                <div class="openseadragon">
                    <xsl:attribute name="id">
                        <xsl:text>openseadragon-{{pos}}</xsl:text>
                    </xsl:attribute>
                </div>
            </div>
        </div>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>


    <!-- Description -->
    <xsl:template name="create_block_description">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="facsimileRef" required="yes" tunnel="yes"/>
        <xsl:text>
{% macro description(pos) %}</xsl:text>
        <xsl:for-each select="$tei/tei:TEI/tei:facsimile[@corresp = $facsimileRef]">
            <xsl:variable name="id" select="concat('facsimile', position())"/>
            <div class="descriptions tab-pane fade" role="tabpanel" tabindex="0">
                <xsl:attribute name="id">
                    <xsl:text>description-{{pos}}</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="aria-labelledby">
                    <xsl:text>description-{{pos}}-tab</xsl:text>
                </xsl:attribute>

                <xsl:for-each select="tei:surface">
                    <xsl:sort data-type="number"
                        select="substring-before(substring-after(@facs, '/f'), '.item')"/>
                    <div class="description">
                        <xsl:attribute name="id">
                            <xsl:text>description-{{pos}}-f</xsl:text>
                            <xsl:value-of
                                select="substring-before(substring-after(@facs, '/f'), '.item')"/>
                        </xsl:attribute>
                        <xsl:attribute name="data-img-id">
                            <xsl:text>f</xsl:text>
                            <xsl:value-of
                                select="substring-before(substring-after(@facs, '/f'), '.item')"/>
                        </xsl:attribute>
                        <xsl:apply-templates/>
                    </div>
                </xsl:for-each>
            </div>
        </xsl:for-each>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>
    
    <!-- Bibliography -->
    <xsl:template name="create_block_biblio">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="facsimileRef" required="yes" tunnel="yes"/>
        <xsl:text>
{% macro biblio(pos) %}</xsl:text>
        <xsl:for-each select="$tei/tei:TEI/tei:facsimile[@corresp = $facsimileRef]">
            <xsl:variable name="id" select="concat('facsimile', position())"/>
            <div class="biblio tab-pane fade" role="tabpanel" tabindex="0">
                <xsl:attribute name="id">
                    <xsl:text>biblio-{{pos}}</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="aria-labelledby">
                    <xsl:text>biblio-{{pos}}-tab</xsl:text>
                </xsl:attribute>
                <i>En cours.</i>
            </div>
        </xsl:for-each>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>

    <xsl:template name="create_block_note">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="facsimileRef" required="yes" tunnel="yes"/>
        <xsl:text>
{% macro notes(pos) %}</xsl:text>

        <div id="zones-notes" style="display:none">
            <xsl:for-each
                select="$tei/tei:TEI/tei:facsimile[@corresp = $facsimileRef]/tei:surface/tei:zone/tei:note">
                <span>
                    <xsl:attribute name="id">
                        <xsl:value-of select="parent::tei:zone/@xml:id"/>
                        <xsl:text>-note</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </span>
            </xsl:for-each>
        </div>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>

    <xsl:template match="tei:desc">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="tei:persName">
        <xsl:variable name="listPerson" select="//tei:person"/>
        <mark class="persName">
            <xsl:apply-templates/>
            
            <!--à changer quand onglets-->
            <span>
                <xsl:attribute name="class">
                    <xsl:text>persName-info fw-normal on-{{pos}}</xsl:text>
                </xsl:attribute>
                <xsl:if test="count(tokenize(@corresp, ' ')) > 1">
                    <i class="d-block text-center">Ambiguïté</i>
                </xsl:if>
                <xsl:for-each select="tokenize(@corresp, ' ')">
                    <xsl:variable name="id" select="replace(., '#', '')"/>
                    <xsl:variable name="person" select="$listPerson[@xml:id = $id]"/>
                    <xsl:choose>
                        <xsl:when test="not(count($listPerson[@xml:id = $id]) = 1)">
                        <xsl:text>Informations à venir</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <b>
                                <xsl:value-of select="$person/tei:persName[@type = 'normalise']"/>
                            </b>
                            <xsl:if test="$person/tei:birth or $person/tei:death">
                                <xsl:text> (</xsl:text>
                                <xsl:call-template name="get_date">
                                    <xsl:with-param name="elem" as="node()">
                                        <xsl:copy-of select="$person/tei:birth"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                                <xsl:text> — </xsl:text>
                                <xsl:call-template name="get_date">
                                    <xsl:with-param name="elem" as="node()">
                                        <xsl:copy-of select="$person/tei:death"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                                <xsl:text>)</xsl:text>
                            </xsl:if>
                            <xsl:if test="$person/tei:occupation">
                                <!-- simulate an <hr/> -->
                                <span class="hr-100">
                                    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                                </span>
                                <xsl:for-each select="$person/tei:occupation">
                                    <!-- todo: notAfter notBefore) -->
                                    <xsl:value-of select="."/>
                                    <xsl:text> (</xsl:text>
                                    <xsl:call-template name="format_date">
                                        <xsl:with-param name="val" select="@from"/>
                                    </xsl:call-template>
                                    <xsl:text>-</xsl:text>
                                    <xsl:call-template name="format_date">
                                        <xsl:with-param name="val" select="@to"/>
                                    </xsl:call-template>
                                    <xsl:text>)</xsl:text>
                                    <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="$person/tei:note and not($person/tei:note = '')">
                                <!-- simulate an <hr/> -->
                                <span class="hr-short">
                                    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                                </span>
                                <xsl:value-of select="$person/tei:note"/>
                            </xsl:if>
                            <xsl:if test="not(position() = last())">
                                <b class="d-block text-center">ou bien</b>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </span>
        </mark>
    </xsl:template>

    <xsl:template match="tei:ref">
        <span>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="contains(@corresp, ' ')">
                        <xsl:call-template name="tokenize">
                            <xsl:with-param name="text" select="@corresp" tunnel="yes"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="substring-after(@corresp, '#')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>

            <xsl:apply-templates/>
            <sup class="description-zone">
                <i class="fa-solid fa-eye"/>
            </sup>
        </span>

    </xsl:template>

    <xsl:template match="tei:zone"/>

    <xsl:template match="tei:note"/>

    <xsl:template name="tokenize">
        <xsl:param name="text" required="yes" tunnel="yes"/>
        <xsl:if test="string-length($text)">
            <xsl:variable name="corr1">
                <xsl:value-of select="substring-after(substring-before($text, ' '), '#')"/>
            </xsl:variable>
            <xsl:variable name="corr2">
                <xsl:value-of
                    select="substring-after(substring-before(substring-after($text, ' '), ' '), '#')"
                />
            </xsl:variable>

            <xsl:choose>

                <xsl:when test="not(contains($text, ' '))">
                    <xsl:value-of select="substring-after($text, '#')"/>
                </xsl:when>

                <xsl:otherwise>

                    <xsl:value-of select="concat($corr1, ' ', $corr2)"/>

                    <xsl:choose>
                        <xsl:when test="not(contains(substring-after($text, ' '), ' '))">
                            <xsl:value-of select="substring-after(substring-after($text, ' '), '#')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text> </xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:call-template name="tokenize">
                        <xsl:with-param name="text"
                            select="substring-after(substring-after($text, ' '), ' ')" tunnel="yes"
                        />
                    </xsl:call-template>
                </xsl:otherwise>

            </xsl:choose>
        </xsl:if>
    </xsl:template>


    <!-- PAS IDEAL CAR IDENTIQUE QUE TEI2CHAPTER -->

    <xsl:template name="get_date">
        <!-- expect <tei:birth/> like element -->
        <xsl:param name="elem"/>
        <xsl:choose>
            <xsl:when test="not($elem = '')">
                <xsl:value-of select="$elem/text()"/>
            </xsl:when>
            <xsl:when test="$elem/@when">
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@when"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$elem/@notBefore and $elem/@notAfter">
                <xsl:text>entre </xsl:text>
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@notBefore"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text> et </xsl:text>
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@notAfter"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$elem/@notBefore and not($elem/@notAfter)">
                <xsl:text>après </xsl:text>
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@notBefore"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="not($elem/@notBefore) and $elem/@notAfter">
                <xsl:text>avant </xsl:text>
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@notAfter"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="format_date">
        <xsl:param name="val"/>
        <xsl:choose>
            <xsl:when test="string-length($val) = 4">
                <xsl:value-of select="xs:integer($val)"/>
            </xsl:when>
            <xsl:when test="string-length($val) = 5 and starts-with($val, '-')">
                <xsl:value-of select="xs:integer(substring($val, 2))"/>
                <xsl:text> av&non_breakable_space;J.C.</xsl:text>
            </xsl:when>
            <xsl:when test="string-length($val) = 10">
                <xsl:value-of select="substring($val, 9, 2)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="substring($val, 6, 2)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="xs:integer(substring($val, 1, 4))"/>
            </xsl:when>
            <xsl:when test="string-length($val) = 11 and starts-with($val, '-')">
                <xsl:value-of select="substring($val, 10, 2)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="substring($val, 7, 2)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="xs:integer(substring($val, 2, 4))"/>
                <xsl:text> av&non_breakable_space;J.C.</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>?</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>



</xsl:stylesheet>
