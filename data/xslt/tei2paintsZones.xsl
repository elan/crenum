<?xml version="1.0" encoding="UTF-8"?><!--
/**

* XSLT for transformation of TEI to JSON : list of zones in paints
* @author RachelGaubil@UGA
* @date : 2024
*/
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="text" encoding="UTF-8" indent="yes"/>

    <xsl:variable name="img_width" select="1600"/>

    <xsl:template match="/">

        <xsl:text>[</xsl:text>
        <xsl:text>{</xsl:text>

        <xsl:for-each select="//tei:surface">

            <xsl:text>"</xsl:text>
            <!--            <xsl:value-of select="@facs"/>-->
            
            <xsl:variable name="data-prefix" select="replace(@facs, '(.*)/ark:/([^/]+/).*', '$1/iiif/ark:/$2')"/>
            <xsl:variable name="data-suffix" select="concat('/full/', $img_width, ',/0/color.jpg')"/>
            <xsl:variable name="data-ident" select="replace(@facs, '.*/ark:/[^/]+/(.*).item', '$1')"/>
            <xsl:value-of select="concat($data-prefix, $data-ident, $data-suffix)"/>
            <xsl:text>":</xsl:text>
            
            <xsl:text>[</xsl:text>
            <xsl:value-of select="@rend"/>
            <xsl:text>,</xsl:text>
            <xsl:text>{</xsl:text>

            <xsl:for-each select="./tei:zone">
                
                <!--si xml-id alors zone et si corresp alors ?-->
                <xsl:if test="@xml:id">
                    <xsl:variable name="points">
                        <xsl:value-of select="@points"/>
                    </xsl:variable>
                    
                    <!--                <xsl:text>{"</xsl:text>-->
                    <xsl:text>"</xsl:text>
                    <xsl:value-of select="@xml:id"/>
                    <xsl:text>":</xsl:text>
                    <xsl:text>[</xsl:text>
                    <xsl:call-template name="tokenize">
                        <xsl:with-param name="text" select="$points" tunnel="yes"/>
                    </xsl:call-template>
                    <xsl:text>]</xsl:text>
                    
                    <!--                <xsl:text>}</xsl:text>-->
                    
                    <xsl:if test="not(position() = last())">
                        <xsl:text>,</xsl:text>
                    </xsl:if>
                </xsl:if>

            </xsl:for-each>

            <xsl:text>}</xsl:text>
            <xsl:text>]</xsl:text>
            

            <xsl:if test="not(position() = last())">
                <xsl:text>,</xsl:text>
            </xsl:if>

        </xsl:for-each>
        
        <xsl:text>}</xsl:text>
        <xsl:text>]</xsl:text>

    </xsl:template>

    <xsl:template name="tokenize">
        <xsl:param name="text" required="yes" tunnel="yes"/>
        <xsl:if test="string-length($text)">
            <xsl:variable name="point">
                <xsl:value-of
                    select="concat(substring-before($text, ' '), substring-before(substring-after($text, ' '), ' '))"/>
            </xsl:variable>
            <xsl:text>{"x":</xsl:text>
            <xsl:value-of select="substring-before($point, ',')"/>
            <xsl:text>,"y":</xsl:text>
            <xsl:value-of select="substring-after($point, ',')"/>
            <xsl:text>}</xsl:text>
            <xsl:if test="contains(substring-after(substring-after($text, ' '), ' '), ' ')">
                <xsl:text>,</xsl:text>
            </xsl:if>
            <xsl:call-template name="tokenize">
                <xsl:with-param name="text"
                    select="substring-after(substring-after($text, ' '), ' ')" tunnel="yes"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
