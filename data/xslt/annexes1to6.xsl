<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation of biography page from XML to HTML
* @author RachelGaubil@UGA
* @date : 2024
*/
-->
<!DOCTYPE biography [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" exclude-result-prefixes="xs"
    xmlns:loext="urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0"
    xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!--Enlève les espaces superflus-->
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">

        <xsl:for-each select="//text:h[@text:outline-level = '2']">

            <xsl:variable name="precedingHnumber">
                <xsl:value-of select="count(preceding::text:h[@text:outline-level = '2']) + 1"/>
            </xsl:variable>

            <xsl:variable name="output-name">
                <xsl:text>carriere_part</xsl:text>
                <xsl:analyze-string select="." regex="\w+\s+(\d)">
                    <xsl:matching-substring>
                        <xsl:value-of select="regex-group(1)"/>
                    </xsl:matching-substring>
                </xsl:analyze-string>
            </xsl:variable>

            <xsl:variable name="content" select="."/>
            <xsl:variable name="output">
                <xsl:text>../../templates/htm/</xsl:text>
                <xsl:value-of select="$output-name"/>
                <xsl:text>.htm</xsl:text>
            </xsl:variable>
            <xsl:result-document method="html" indent="yes" encoding="UTF-8"
                omit-xml-declaration="yes" href="{$output}">

                <xsl:variable name="titleContent">
                    <xsl:apply-templates/>
                </xsl:variable>

                <xsl:variable name="title">
                    <xsl:choose>
                        <xsl:when test="contains($titleContent, '(')">
                            <xsl:value-of
                                select="replace(substring-after(substring-before($titleContent, '('), ': '), '\s+', ' ')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="replace(substring-after($titleContent, ': '), '\s+', ' ')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <!--ne met pas les italiques par exemple, si besoin d'appliquer les templates il faut enlever "annexe 2" du titre dans les données-->
                <h2 class="title">
                    <xsl:value-of select="$title"/>
                </h2>

                <xsl:for-each select="following-sibling::*">

                    <xsl:if test="not(name() = 'text:h' and @text:outline-level = '2')">
                        <xsl:variable name="currentHnumber">
                            <xsl:value-of
                                select="count(preceding-sibling::text:h[@text:outline-level = '2'])"
                            />
                        </xsl:variable>

                        <xsl:if test="$currentHnumber = $precedingHnumber">
                            <xsl:apply-templates select="."/>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
                <xsl:call-template name="create_block_note"/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="transform_annexes">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template name="create_block_note">
        <div id="notes" role="tabpanel" tabindex="0" aria-labelledby="notes">
            <xsl:apply-templates mode="note" select="//text:note/text:note-body"/>
        </div>
    </xsl:template>

    <xsl:template match="text:h">
        <xsl:choose>
            <!--TITLE STYLE-->
            <xsl:when test="@text:outline-level = 2">
                <h3>
                    <xsl:apply-templates/>
                </h3>
            </xsl:when>
            <xsl:when test="@text:outline-level = 3">
                <h4>
                    <xsl:apply-templates/>
                </h4>
            </xsl:when>
            <xsl:otherwise>
                <!--Ne pas prendre le titre de la table des matières-->
                <xsl:if test="@text:style-name != 'P8'">
                    <span class="red">
                        <xsl:apply-templates/>
                    </span>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text:p">
        <xsl:choose>
            <xsl:when test="ancestor::table:table">
                <!--TABLE STYLE-->
                <span>
                    <xsl:if test="@text:style-name = 'P28'">
                        <xsl:attribute name="class">
                            <xsl:text>fw-bold</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <xsl:when test="@loext:marker-style-name = 'T42'">
                <h5>
                    <xsl:apply-templates/>
                </h5>
            </xsl:when>
            <xsl:when test=".//text:tab">
                <p>
                    <xsl:apply-templates select=".//text:tab/preceding-sibling::node()"/>
                    <span class="tab"/>
                    <xsl:apply-templates select=".//text:tab/following-sibling::node()"/>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:choose>
                        <xsl:when
                            test="(@text:style-name = 'P2' and text() != '') or @text:style-name = 'P24'">
                            <xsl:attribute name="class">
                                <xsl:text>text-center</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <!--CITATION STYLE-->
                        <xsl:when test="@text:style-name = 'Verger_5f_citation_20_longue'">
                            <xsl:attribute name="class">
                                <xsl:text>smaller quote</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="@text:style-name = 'P18'">
                            <xsl:attribute name="class">
                                <xsl:text>smaller text-center quote</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="@text:style-name = 'P21'">
                            <xsl:attribute name="class">
                                <xsl:text>smaller text-center small-caps</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <!-- LIST -->
                        <!-- list already numbered -->
                        <xsl:when test="@text:style-name = 'P5'">
                            <xsl:attribute name="class">
                                <xsl:text>double-indent</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:apply-templates/>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--TEXT STYLE-->
    <xsl:template match="text:span">
        <xsl:choose>
            <!--italic + italic-->
            <xsl:when
                test="@text:style-name = 'T12' or @text:style-name = 'T13' or @text:style-name = 'T14' or @text:style-name = 'T15' or @text:style-name = 'T19' or @text:style-name = 'T20' or @text:style-name = 'T30' or @text:style-name = 'T31' or @text:style-name = 'T35' or @text:style-name = 'T39'">
                <span class="fst-italic">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <!--Test de prendre tout ce qu'on trouve dans l'en-tête-->
            <!--italic + small-caps-->
            <xsl:when test="@text:style-name = 'T4'">
                <span class="fst-italic small-caps">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <!--italic + SUPER-->
            <xsl:when test="@text:style-name = 'T23' or @text:style-name = 'T24'">
                <span class="fst-italic">
                    <sup>
                        <xsl:apply-templates/>
                    </sup>
                </span>
            </xsl:when>
            <!--italic + bold-->
            <xsl:when test="@text:style-name = 'T16'">
                <span class="fst-italic fw-bold">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <!--small-caps (c'est bien du small-capset pas de la majuscule même pour les chiffres romains-->
            <xsl:when test="@text:style-name = 'T1' or @text:style-name = 'T8'">
                <span class="small-caps">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <!--small-caps +sup-->
            <xsl:when test="@text:style-name = 'T6' or @text:style-name = 'T7'">
                <span class="small-caps">
                    <sup>
                        <xsl:apply-templates/>
                    </sup>
                </span>
            </xsl:when>
            <!--sup-->
            <xsl:when test="@text:style-name = 'T22' or @text:style-name = 'T25'">
                <sup>
                    <xsl:apply-templates/>
                </sup>
            </xsl:when>
            <!--bold-->
            <xsl:when
                test="@text:style-name = 'T26' or @text:style-name = 'T27' or @text:style-name = 'T28' or @text:style-name = 'T41'">
                <span class="fw-bold">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <!--underlined-->
            <xsl:when test="@text:style-name = 'T37'">
                <span class="text-decoration-underline">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="table:table">
        <table class="table table-bordered">
            <xsl:apply-templates/>
        </table>
    </xsl:template>

    <xsl:template match="table:table-header-rows">
        <thead>
            <xsl:apply-templates/>
        </thead>
    </xsl:template>

    <xsl:template match="table:table-row">
        <tr>
            <!-- Si pas dans la ligne de titre + on est dans tableau 3 ou 4 -->
            <xsl:if
                test="not(ancestor::table:table-header-rows) and (ancestor::table:table/@table:name = 'Table6' or ancestor::table:table/@table:name = 'Table9')">
                <xsl:variable name="colNb">
                    <xsl:value-of select="count(table:table-cell)"/>
                </xsl:variable>
                <!-- Si pas de colonnes fusionnées + oui -->
                <xsl:if test="$colNb = 4">
                    <xsl:if
                        test="(./table:table-cell[2]/text:p/text() != '' and ./table:table-cell[2]/text:p/text() != 'Non' and ./table:table-cell[2]/text:p/text() != 'Absent' and ./table:table-cell[2]/text:p/text() != 'S.P.') or (./table:table-cell[3]/text:p/text() != '' and ./table:table-cell[3]/text:p/text() != 'Non' and ./table:table-cell[3]/text:p/text() != 'Absent' and ./table:table-cell[3]/text:p/text() != 'S.P.')">
                        <xsl:attribute name="class">
                            <xsl:text>present</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                </xsl:if>
            </xsl:if>
            <xsl:apply-templates/>
        </tr>
    </xsl:template>

    <xsl:template match="table:table-cell">
        <xsl:choose>
            <xsl:when
                test="ancestor::*[1 and name() = 'table:table-row' and (ancestor::*[1 and name() = 'table:table-header-rows'])]">
                <th>
                    <xsl:if
                        test="ancestor::table:table/@table:name = 'Table6' or ancestor::table:table/@table:name = 'Table9'">
                        <xsl:attribute name="class">
                            <xsl:text>col-3</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates/>
                </th>
            </xsl:when>
            <xsl:otherwise>
                <td>
                    <xsl:if test="@table:number-columns-spanned">
                        <xsl:attribute name="colspan">
                            <xsl:value-of select="@table:number-columns-spanned"/>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates/>
                </td>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--<xsl:template match="text:note">
        <xsl:if test="not(@text:note-class='footnote')">
            <xsl:apply-templates/>
        </xsl:if>
    </xsl:template>-->

    <xsl:template match="text:note-citation">
        <xsl:variable name="num">
            <xsl:value-of select="./text()"/>
        </xsl:variable>
        <sup class="modal-call note-call" aria-hidden="true" id="noteCall{$num}"
            data-modal-id="noteModal{$num}">
            <xsl:apply-templates/>
        </sup>
    </xsl:template>

    <!--    pour l'instant rien pour les notes-->
    <xsl:template match="text:note-body"/>
    <xsl:template match="text:note-body" mode="note">
        <xsl:variable name="num">
            <xsl:value-of select="preceding-sibling::text:note-citation/text()"/>
        </xsl:variable>
        <h4 class="modal-title" modal-title="noteModal{$num}">Note n°<xsl:value-of select="$num"
            /></h4>
        <div class="modal-text" modal-text="noteModal{$num}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="text:list">
        <xsl:choose>
            <!-- list already numbered -->
            <xsl:when test="following-sibling::text:p[1][@text:style-name = 'P5'] or preceding-sibling::text:p[1][@text:style-name = 'P5']">
                <p>
                    <xsl:apply-templates/>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <ul>
                    <xsl:apply-templates/>
                </ul>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text:list-item">
        <xsl:choose>
            <!-- list already numbered -->
            <xsl:when test="parent::text:list[following-sibling::text:p[1][@text:style-name = 'P5']] or parent::text:list[preceding-sibling::text:p[1][@text:style-name = 'P5']]">
                <p>
                    <xsl:apply-templates/>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <li>
                    <xsl:apply-templates/>
                </li>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text:line-break">
        <br/>
    </xsl:template>

    <xsl:template match="draw:frame">
        <img src="/static/img/annexe.png"/>
    </xsl:template>

    <xsl:template match="text:table-of-content"/>


</xsl:stylesheet>
