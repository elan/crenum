# Run transformations for search
## NB. XML input file is irrelevant (-s option). XSL are looking for collection('../xml/?select=Livre_*.xml')
saxonb-xslt -ext:on -xsl:tei2search_template.xsl -s:../xml/Livre_I.xml output=../../templates/search.html.j2
saxonb-xslt -ext:on -xsl:tei2search_jsonIndex.xsl -s:../xml/Livre_I.xml output=../../static/json/auto_index.json
saxonb-xslt -ext:on -xsl:tei2search_appSetup.xsl -s:../xml/Livre_I.xml output=../../static/js/auto_app_minisearch.js


