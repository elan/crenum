<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation of TEI to HTML : one page by book, chapter by chapter
* @author AnneGF@CNRS
* @date : 2022-2023
*/
-->

<!DOCTYPE tei2editorial [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>


    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template name="check">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:if test="count($tei//@facs[preceding::*/@facs = .]) > 0">
            <xsl:message>
                <xsl:text>Attention, il y a des @facs en doublon : </xsl:text>
                <xsl:for-each select="$tei//@facs[preceding::*/@facs = .]">
                    <xsl:value-of select="."/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
            </xsl:message>
        </xsl:if>
        
    </xsl:template>

</xsl:stylesheet>
