<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation of TEI to JSON
* @author RachelGaubil@UGA
* @date : 2024
*/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" exclude-result-prefixes="xs"
    version="2.0">

    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>
    <!-- Ignore les éléments ne contenant que des espaces et les tabulations-->
    <xsl:output method="text" encoding="UTF-8"/>


    <xsl:template match="/">

        <xsl:text>[</xsl:text>
        
        <xsl:variable name="totalCell">
            <xsl:value-of select="count(//table:table-row/table:table-cell[(not(@table:number-columns-spanned) or 50 != @table:number-columns-spanned) and not(@table:number-columns-repeated) and child::text:p])"/>
        </xsl:variable>

        <xsl:variable name="firstYear">
            <xsl:value-of select="//table:table/table:table-row[2]/table:table-cell[1]"/>
        </xsl:variable>

        <xsl:for-each select="//table:table-row[position() > 2]">

            <xsl:for-each select="./table:table-cell">
                
                <xsl:variable name="cellNum">
                    <xsl:value-of select="count(preceding::table:table-cell[(not(@table:number-columns-spanned) or 50 != @table:number-columns-spanned) and not(@table:number-columns-repeated) and child::text:p]) + 1"/>
                </xsl:variable>
                
                <!-- somme des colonnes vides et fusionnées -->
                <xsl:variable name="precedingYears">
                    <xsl:value-of select="sum(preceding-sibling::table:table-cell[@table:number-columns-spanned]/@table:number-columns-spanned) + sum(preceding-sibling::table:table-cell[51 > @table:number-columns-repeated]/@table:number-columns-repeated)"/>
                </xsl:variable>
                
                <xsl:variable name="startYear">
                    <xsl:value-of select="$firstYear + count(preceding-sibling::table:table-cell[not(count(preceding-sibling::table:table-cell))]) + $precedingYears"/>
                </xsl:variable>
                
                <xsl:if test="./text:p and (not(@table:number-columns-spanned) or @table:number-columns-spanned != 50)">
                    
                    <xsl:text>{"</xsl:text>
                    
                    <!-- title -->
                    <xsl:text>id":"</xsl:text>
                    <!-- 50 = nb d'années et donc de cellules dans la première ligne => permet de démarrer les id à 1 -->
                    <xsl:value-of select="$cellNum - 50"/>
                    <xsl:text>","</xsl:text>
                    
                    <!-- title -->
                    <xsl:text>title":"</xsl:text>
                    <xsl:value-of select="preceding::table:table-cell[@table:number-columns-spanned = 50][1]"/>
                    <xsl:text>","</xsl:text>
                    
                    <!-- start year -->
                    <xsl:text>start":"</xsl:text>
                    <xsl:value-of select="$startYear"/>
                    <xsl:text>","</xsl:text>
                    
                    <!-- end year -->
                    <xsl:text>end":"</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@table:number-columns-spanned">
                            <xsl:value-of select="$startYear + @table:number-columns-spanned"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$startYear + 1"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>","</xsl:text>
                    
                    <!-- name -->
                    <xsl:text>name":"</xsl:text>
                    <xsl:choose>
                        <xsl:when test="count(text:p) > 1">
                            <xsl:for-each select="./text:p">
                                <xsl:value-of select="text()"/>
                                <xsl:if test="position() != last()">
                                    <xsl:text>\n</xsl:text>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>                            
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:text>"}</xsl:text>
                    
                    <!-- compare le nombre de cellule avec texte précédentes (+1 pour l'actuelle) avec le nombre total pour savoir si on est bien sur la dernière -->
                    <xsl:if test="$cellNum != $totalCell">
                        <xsl:text>,</xsl:text>
                    </xsl:if>
                    
                </xsl:if>

            </xsl:for-each>

        </xsl:for-each>

        <xsl:text>]</xsl:text>

    </xsl:template>

</xsl:stylesheet>
