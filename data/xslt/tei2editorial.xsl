<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation of editorial pages from TEI to HTML, initialy designed for CorrProust Project
* @author AnneGF@CNRS
* @date : 2022-20222
*/
-->

<!DOCTYPE corr-proust_tei2editorial [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:sf="http://saxon.sf.net/" xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs tei sf" version="2.0">

    <xsl:output method="html" indent="yes"/>
    
    <!--Enlève les espaces superflus-->
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">
        <xsl:apply-templates select="tei:TEI/tei:text"/>
    </xsl:template>

    <xsl:template match="tei:text">
        <xsl:apply-templates select="tei:body/*"/>
    </xsl:template>

    <xsl:template match="tei:div">
        <xsl:variable name="div-cpt"><xsl:number count="tei:div" level="any"/></xsl:variable>
        <xsl:choose>
            <!-- si la dernière div croisée (donc parent) n'est pas la première div du body -> alors div -->
            <xsl:when test="$div-cpt != 1">
                <div>
                    <xsl:apply-templates/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:lb">
        <br/>
    </xsl:template>
    <xsl:template match="tei:title">
        <span class="fst-italic">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:head">
        <xsl:variable name="div-cpt"><xsl:number count="tei:div" level="any"/></xsl:variable>
        <xsl:variable name="head-cpt"><xsl:number count="tei:head" level="any"/></xsl:variable>
        <xsl:choose>
            <!-- si le dernier head croisé est la premièr head du body -> alors grand titre -->
            <xsl:when test="$head-cpt = 1">
                <xsl:element name="h2">
                    <xsl:attribute name="class"><xsl:text>title</xsl:text></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <!-- si la dernière div croisée (donc parent) est la première div du body -> alors chapeau -->
            <xsl:when test="$div-cpt = 1">
                <xsl:element name="h3">
                    <xsl:attribute name="class"><xsl:text>title</xsl:text></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="h4">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic'] | tei:hi[@rend = 'it']">
        <span class="fst-italic">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'superscript']">
        <sup>
            <xsl:apply-templates/>
        </sup>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'bold']">
        <span class="fw-bold">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a href="{@target}">
            <!-- si lien externe -->
            <xsl:if test="contains(@target, 'http')">
                <xsl:attribute name="target"><xsl:text>_blank</xsl:text></xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    <xsl:template match="tei:list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:figure">
        <!-- POURQUOI ALT PAS DE VALEUR -->
        <img alt="{tei:figDesc}" src="{tei:graphic/@url}">
            <xsl:choose>
                <xsl:when test="@place = 'inline right'">
                    <xsl:attribute name="class">
                        <xsl:text>inline-right</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@place = 'inline left'">
                    <xsl:attribute name="class">
                        <xsl:text>inline-left</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@place = 'center'">
                    <xsl:attribute name="class">
                        <xsl:text>center</xsl:text>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
        </img>
    </xsl:template>
    <xsl:template match="tei:email">
        <mark>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:name">
        <span class="small-caps">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="*">
        <span class="fw-bold red">
            <xsl:value-of select="name(.)"/>
        </span>
    </xsl:template>
</xsl:stylesheet>
