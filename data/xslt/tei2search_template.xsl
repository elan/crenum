<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT TEI to Json : index for MiniSearch
* @author AnneGF@CNRS
* @date : 2024
* TODO : all books
* XML : Livre I.xml (à terme à changer donc...)
* Output : templates/search.html.j2

*/
-->

<!DOCTYPE xsl [
    <!ENTITY times "&#215;">
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:param name="books" select="collection('../xml/?select=Livre_*.xml')"/>

    <xsl:param name="output">
        <xsl:text>../../templates/search.html.j2</xsl:text>
    </xsl:param>

    <xsl:variable name="br">
        <xsl:text>
</xsl:text>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:variable name="all_witness_id"
            select="distinct-values($books//@wit/tokenize(replace(replace(normalize-space(.), '&gt;', '_'), '#', ''), ' '))"/>

        <xsl:result-document href="{$output}">
            <xsl:text>{% extends "base.html.j2" %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block css %}</xsl:text>
            <link href="/static/css/app_minisearch.css" rel="stylesheet"/>
            <xsl:text>{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block containertype %}container{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block pageTitle %}Recherche{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block body %}</xsl:text>
            <xsl:value-of select="$br"/>

            <div>
                <h3>Chercher dans un vers...</h3>
                <div id="app">
                    <div class="App">
                        <div class="Loader">chargement des données...</div>
                        <div class="SearchBox">
                            <div class="Search">
                                <input type="text"/>
                                <button class="clear">×</button>
                            </div>
                            <ul class="SuggestionList"/>
                            <details class="AdvancedOptions p-4 pt-2 pb-2">
                                <summary>Options avancées</summary>
                                <form class="options">
                                    <div id="witnesses_selection">
                                        <b>Chercher dans les manuscrits&nbsp;:</b>
                                        <xsl:variable name="click_all">
                                            <xsl:text>var checkboxes = document.getElementsByName("fields"); for (var checkbox of checkboxes) { checkbox.checked = true; }; </xsl:text>
                                            <xsl:text>const query = $searchInput.value;  const results = getSearchResults(query); renderSearchResults(results);</xsl:text>
                                        </xsl:variable>
                                        <xsl:variable name="click_none">
                                            <xsl:text>var checkboxes = document.getElementsByName("fields"); for (var checkbox of checkboxes) { checkbox.checked = false; };</xsl:text>
                                            <xsl:text>const query = $searchInput.value;  const results = getSearchResults(query); renderSearchResults(results);</xsl:text>
                                        </xsl:variable>
                                        <xsl:variable name="click_reverse">
                                            <xsl:text>var checkboxes = document.getElementsByName("fields"); for (var checkbox of checkboxes) { if (checkbox.checked) { checkbox.checked = false; } else { checkbox.checked = true; } };</xsl:text>
                                            <xsl:text>const query = $searchInput.value;  const results = getSearchResults(query); renderSearchResults(results);</xsl:text>
                                        </xsl:variable>
                                        <span class="btn btn-sm btn-light p-0 m-0 px-1 mx-1 " onclick="{$click_all}">Tout sélectionner</span>
                                        <span class="btn btn-sm btn-light p-0 m-0 px-1 mx-1" onclick="{$click_none}">Tout désélectionner</span>
                                        <span class="btn btn btn-sm btn-light p-0 m-0 px-1 mx-1" onclick="{$click_reverse}">Inverser la sélection</span>
                                        
                                        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
                                        <label>
                                            <b><input type="checkbox" name="fields" value="lem"
                                                  checked=""/>&nbsp;BnFfr 2817</b>
                                        </label>
                                        <xsl:for-each select="$all_witness_id">
                                            <xsl:sort/>
                                            <xsl:variable name="wit" select="replace(., '#', '')"/>
                                            <label class="wit"><input type="checkbox" name="fields"
                                                  value="{$wit}"/>&nbsp;<xsl:value-of select="$wit"
                                                /></label>
                                            <xsl:text> </xsl:text>
                                        </xsl:for-each>
                                    </div>
                                    <div>
                                        <b>Élargir la recherche</b>
                                        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
                                        <label><input type="checkbox" name="prefix" value="false"/>
                                            Rechercher les mots qui commencent par... (par exemple
                                                <code>qui</code> renverra <code>qui</code>,
                                                <code>quidem</code>, etc.)</label>
                                        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
                                        <label><input type="checkbox" name="fuzzy" value="false"/>
                                            Autoriser les résultats approximatifs</label>
                                    </div>
                                    <div>
                                        <b>Rechercher</b>
                                        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
                                        <label><input type="radio" name="combineWith" value="OR"/>
                                            au moins un des termes (<code>roy</code> ou
                                                <code>ost</code>)</label>
                                        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
                                        <label><input type="radio" name="combineWith" value="AND"
                                                checked=""/> tous les termes (<code>roy</code> et
                                                <code>ost</code>)</label>
                                    </div>
                                </form>
                            </details>
                        </div>
                        <div class="m-4">
                            <p class="Explanation">Votre recherche n'a donné aucun résultat. Grâce
                                aux options avancées, vous pouvez élargir votre recherche en
                                cherchant <i>les mots commençant par</i>, en autorisant les
                                    <i>résultats approximatifs</i> et en cherchant pour différents
                                témoins.</p>
                        </div>
                        <ul class="resultsList"/>
                    </div>
                </div>
            </div>

            <script src="https://cdn.jsdelivr.net/npm/minisearch@7.1.0/dist/umd/index.min.js"/>
            <script src="/static/js/auto_app_minisearch.js"/>
            <script src="/static/js/app_minisearch.js"/>
            <xsl:value-of select="$br"/>
            <xsl:text>{% endblock %}</xsl:text>
        </xsl:result-document>
    </xsl:template>

</xsl:stylesheet>
