window.onload = function() {
  modalHandler.init();
}

const modalHandler = {
  init: function() {
    let modalCalls = document.querySelectorAll('.modal-call');
    [].forEach.call(modalCalls, function(modalCall) {
      modalCall.addEventListener("click", modalHandler.modalCall);
    });
  },
  modalCall: function(evt) {
    console.log("test")
    evt.stopPropagation();
    let modalCall = evt.currentTarget;
    let modalId = modalCall.dataset.modalId;
    console.log(modalId);

    let modalNb = document.querySelector('[modal-title="' + modalId + '"]').innerHTML;
    let modalText = document.querySelector('[modal-text="' + modalId + '"]').innerHTML;
    document.querySelector("#modal .modal-text").innerHTML = modalText;
    document.querySelector("#modal-title-id").innerHTML = modalNb;

    new bootstrap.Modal(document.getElementById("modal"), {}).toggle();
  }
}
