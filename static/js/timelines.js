window.onload = function() {
  // intiate Cretin titles timeline
  if(document.getElementById('cretinTitlesVisualization')){
    timeline.init("cretinTitlesVisualization", cretinTitlesHandler);
  }
  // intiate kings timeline
  else if(document.getElementById('kingsVisualization')){
    timeline.init("kingsVisualization", kingsHandler);
  }
}

let timeline = {
  init: function(containerId, handlerName) {
    handlerName.init();
    // Configuration for the Timeline
    handlerName.options.width = '100%';
    handlerName.options.orientation = {
      axis: 'top'
    };

    setTimeout(() => {
      // DOM element where the Timeline will be attached
      var container = document.getElementById(containerId);
      // Initiate timeline
      var timeline = new vis.Timeline(container, handlerName.items, handlerName.groups, handlerName.options);
    }, "1000");
  }
}

let kingsHandler = {
  listReigns: "../static/json/list_reigns.json",
  copyListReigns: Object(),
  items: new vis.DataSet(),
  groups: new vis.DataSet(),
  options: new vis.DataSet(),
  // options: {
  //   min: "1460-01-01",
  //   max: "1530-01-01",
  // },
  titlesGroup: [
    "Roi de France",
    "Roi des Francs"
  ],
  titlesList: [
    "Roi de France",
    "Roi des Francs",
    "Roi des Francs saliens",
    "Roi des Francs de Metz",
    "Roi des Francs de Paris",
    "Roi des Francs d'Orléans",
    "Roi des Francs d'Austrasie",
    "Roi des Francs de Neustrie",
    "Roi des Francs de Bourgogne",
    "Roi des Francs d'Austrasie et de Bourgogne",
    "Roi des Lombards"
  ],
  titlesId: {},
  kingsList: [],
  // liste des reignes traités pour éviter les doublons quand deux mêmes règnes
  reignIds: [],
  normalizeDate: function(date) {
    var regexOnlyYear = /^\d\d\d+$/gm;
    if (regexOnlyYear.test(date)) {
      date = "0" + date + "-01-01";
    }
    return date;
  },
  init: function() {
    
    // initiate titlesId ("reignName": groupId)
    titleCnt = 1;
    for(var i = 0; i < kingsHandler.titlesList.length; i++){
      if (kingsHandler.titlesGroup.includes(kingsHandler.titlesList[i])){
        kingsHandler.titlesId[kingsHandler.titlesList[i]] = 1;
      }
      else {
        titleCnt += 1;
        kingsHandler.titlesId[kingsHandler.titlesList[i]] = titleCnt;
      }
    }

    // add groups
    kingsHandler.groups.add({
      id: 1,
      content: "Roi de France / Roi des Francs",
      subgroupStack: {'severalReigns': true, "oneReign": false}
    });
    for (var i = 0; i < kingsHandler.titlesList.length; i++) {
      var id = kingsHandler.titlesId[kingsHandler.titlesList[i]];
      if (id != 1){
        kingsHandler.groups.add({
          id: id,
          content: kingsHandler.titlesList[i],
          subgroupStack: {'severalReigns': true, "oneReign": false}
        });
      }
    }

    $.get(kingsHandler.listReigns, function(reigns) {

      kingsHandler.copyListReigns = reigns;

      var kingNb = 0;
      for (var i = 0; i < reigns.length; i++) {
        
        // on retire le reigne courant de la copie de la liste
        kingsHandler.copyListReigns = kingsHandler.copyListReigns.slice(1);
        var reignId = reigns[i]["id"];

        // ne traiter que les reignes pas déjà traités
        if (kingsHandler.reignIds.findIndex((element) => element == reignId) == -1) {

          kingsHandler.reignIds.push(reignId);

          var start = reigns[i]["start"];
          var end = reigns[i]["end"];

          if (start != null && end != null) {

            var id = i + 1;
            var content = reigns[i]["name"];
            var title = reigns[i]["title"];
            var groupId = kingsHandler.titlesId[title];

            // vérifier si pas un doublons de reigne, si oui alors l'indiquer
            var severalReigns = false;
            for (var copyReign in kingsHandler.copyListReigns) {
              var copyTitle = kingsHandler.copyListReigns[copyReign]["title"];
              var copyGroupId = kingsHandler.titlesId[copyTitle];
              var copyStart = kingsHandler.copyListReigns[copyReign]["start"];
              var copyEnd = kingsHandler.copyListReigns[copyReign]["end"];
              if (start == copyStart && end == copyEnd && groupId == copyGroupId) {
                severalReigns = true;
              }
            }

            // list kings to have one color per king
            if (kingsHandler.kingsList.findIndex((element) => element == content) == -1) {
              kingsHandler.kingsList.push(content);
            }
            kingNb = kingsHandler.kingsList.findIndex((element) => element == content)+1;

            // add items
            if (severalReigns) {
              kingsHandler.items.add({
                id: id,
                content: content,
                start: start,
                end: end,
                group: groupId,
                subgroup: "severalReigns",
                className: 'king' + kingNb
              });
            } else {
              kingsHandler.items.add({
                id: id,
                content: content,
                start: start,
                end: end,
                group: groupId,
                subgroup: "oneReign",
                className: 'king' + kingNb
              });
            }
          }
        }
      }
    });
  }
}

let cretinTitlesHandler = {
  titlesList: "../static/json/list_cretin_titles.json",
  titlesGroups: [],
  items: new vis.DataSet(),
  groups: new vis.DataSet(),
  options: {
    min: "1460-01-01",
    max: "1530-01-01",
  },
  
  init: function() {

    // get json content
    $.get(cretinTitlesHandler.titlesList, function(titles) {

        // list groups
      for(var i = 0; i < titles.length; i++){
        if (!cretinTitlesHandler.titlesGroups.includes(titles[i]["title"])){
          cretinTitlesHandler.titlesGroups.push(titles[i]["title"]);
        }
      }
      // add groups
      for(var i = 0; i < cretinTitlesHandler.titlesGroups.length; i++){
        cretinTitlesHandler.groups.add({
          id: i + 1,
          content: cretinTitlesHandler.titlesGroups[i],
          subgroupStack: true
        });
      }

      // add items
      for(var i = 0; i < titles.length; i++){
        var groupId = cretinTitlesHandler.titlesGroups.indexOf(titles[i]["title"]);

        // to display title when hovering items
        var item = document.createElement('span');
        item.setAttribute("title",titles[i]["name"]);
        item.innerHTML = titles[i]["name"];

        cretinTitlesHandler.items.add({
          id: titles[i]["id"],
          content: item,
          start: titles[i]["start"] + "-01-01",
          end: titles[i]["end"] + "-01-01",
          group: groupId
        });
      }
    });
  }
}
