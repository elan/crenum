// Setup data
let $searchable_fields = ['lem','BnFfr23146', 'Aix419', 'BnFfr4964', 'BnFfr17274', 'BnFfr4965', 'Cha515', 'et', 'BnFfr23148', 'BnFfr231146', '_BnFfr23146',
'BnFfr21316', 'BnF4965', 'BnDfr4964', 'BnFfr24136', 'Aix1419', 'BnFfr17174', 'Aix410', 'BnFfr23136', 'BnFr23146', 'BnFfr4967',
'BnFfr5299', 'BnFfr23145', 'Cha514', 'Vat966'];
let $showable_fields = ['type', 'book', 'chapter', 'n', 'lem','BnFfr23146', 'Aix419', 'BnFfr4964', 'BnFfr17274', 'BnFfr4965', 'Cha515', 'et', 'BnFfr23148', 'BnFfr231146', '_BnFfr23146',
'BnFfr21316', 'BnF4965', 'BnDfr4964', 'BnFfr24136', 'Aix1419', 'BnFfr17174', 'Aix410', 'BnFfr23136', 'BnFr23146', 'BnFfr4967',
'BnFfr5299', 'BnFfr23145', 'Cha514', 'Vat966'];

// Setup MiniSearch
let miniSearch = new MiniSearch({
fields: $searchable_fields, // fields to index for full-text search
storeFields: $showable_fields // fields to return with search results
})

let $data_json = 'static/json/auto_index.json';

const renderSearchResults = (results, searchOptions = getSearchOptions()) => {
    //JSON.stringify(results);
    const books = new Set(results.map(a => a.book));
    let $result;
    $resultsList.innerHTML = "";
    $resultsList.innerHTML += "<i class='small'>"+results.length+" résultat(s) trouvé(s)</i>";
    for (let b of books){
        $resultsList.innerHTML += "<h4>Livre "+b+"</h4>\n";
        results_by_book = results.filter((result) => result.book == b); //filter by book
        results_by_book.sort((a,b) => { //sort by verse number
          return (a.n - b.n);
        });

    $resultsList.innerHTML += results_by_book.map(({ type, book, chapter, n, lem,BnFfr23146, Aix419, BnFfr4964, BnFfr17274, BnFfr4965, Cha515, et, BnFfr23148, BnFfr231146, _BnFfr23146, BnFfr21316, BnF4965,
BnDfr4964, BnFfr24136, Aix1419, BnFfr17174, Aix410, BnFfr23136, BnFr23146, BnFfr4967, BnFfr5299, BnFfr23145, Cha514, Vat966 }) => {
    let $chap = `${chapter}`.substring(`${chapter}`.lastIndexOf("_")+1);
    if ( `${type}` === "verse"){
      $result='<h5 xmlns="http://www.w3.org/1999/xhtml" class="mt-3">Vers <a href="';
      $result+=`/book${book}/${chapter}#${n}">${n}</a> (chapitre `
      $result+=$chap;
      $result+=`)</h5>`;    }
    if ( `${type}` === "page_header"){
      $result='<h5 xmlns="http://www.w3.org/1999/xhtml" class="mt-3">Titre courant du folio <a href="';
      $result+=`/book${book}/${chapter}/`+`${n.replace('f','p')}`+`/image-left_linear-right">${n}</a> (chapitre `
      $result+=$chap;
      $result+=`)</h5>`;
    }
$result+=`<dl xmlns="http://www.w3.org/1999/xhtml">
   <dt>BnFfr2817 :</dt>
   <dd>${lem}</dd>`
    if (! (`${BnFfr23146}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr23146') == -1)) {
      $result += `
   <dt>BnFfr23146 : </dt>
   <dd>${BnFfr23146}</dd>
    `}
    if (! (`${Aix419}` === "undefined") && ! (searchOptions.fields.indexOf('Aix419') == -1)) {
      $result += `
   <dt>Aix419 : </dt>
   <dd>${Aix419}</dd>
    `}
    if (! (`${BnFfr4964}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr4964') == -1)) {
      $result += `
   <dt>BnFfr4964 : </dt>
   <dd>${BnFfr4964}</dd>
    `}
    if (! (`${BnFfr17274}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr17274') == -1)) {
      $result += `
   <dt>BnFfr17274 : </dt>
   <dd>${BnFfr17274}</dd>
    `}
    if (! (`${BnFfr4965}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr4965') == -1)) {
      $result += `
   <dt>BnFfr4965 : </dt>
   <dd>${BnFfr4965}</dd>
    `}
    if (! (`${Cha515}` === "undefined") && ! (searchOptions.fields.indexOf('Cha515') == -1)) {
      $result += `
   <dt>Cha515 : </dt>
   <dd>${Cha515}</dd>
    `}
    if (! (`${et}` === "undefined") && ! (searchOptions.fields.indexOf('et') == -1)) {
      $result += `
   <dt>et : </dt>
   <dd>${et}</dd>
    `}
    if (! (`${BnFfr23148}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr23148') == -1)) {
      $result += `
   <dt>BnFfr23148 : </dt>
   <dd>${BnFfr23148}</dd>
    `}
    if (! (`${BnFfr231146}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr231146') == -1)) {
      $result += `
   <dt>BnFfr231146 : </dt>
   <dd>${BnFfr231146}</dd>
    `}
    if (! (`${_BnFfr23146}` === "undefined") && ! (searchOptions.fields.indexOf('_BnFfr23146') == -1)) {
      $result += `
   <dt>_BnFfr23146 : </dt>
   <dd>${_BnFfr23146}</dd>
    `}
    if (! (`${BnFfr21316}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr21316') == -1)) {
      $result += `
   <dt>BnFfr21316 : </dt>
   <dd>${BnFfr21316}</dd>
    `}
    if (! (`${BnF4965}` === "undefined") && ! (searchOptions.fields.indexOf('BnF4965') == -1)) {
      $result += `
   <dt>BnF4965 : </dt>
   <dd>${BnF4965}</dd>
    `}
    if (! (`${BnDfr4964}` === "undefined") && ! (searchOptions.fields.indexOf('BnDfr4964') == -1)) {
      $result += `
   <dt>BnDfr4964 : </dt>
   <dd>${BnDfr4964}</dd>
    `}
    if (! (`${BnFfr24136}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr24136') == -1)) {
      $result += `
   <dt>BnFfr24136 : </dt>
   <dd>${BnFfr24136}</dd>
    `}
    if (! (`${Aix1419}` === "undefined") && ! (searchOptions.fields.indexOf('Aix1419') == -1)) {
      $result += `
   <dt>Aix1419 : </dt>
   <dd>${Aix1419}</dd>
    `}
    if (! (`${BnFfr17174}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr17174') == -1)) {
      $result += `
   <dt>BnFfr17174 : </dt>
   <dd>${BnFfr17174}</dd>
    `}
    if (! (`${Aix410}` === "undefined") && ! (searchOptions.fields.indexOf('Aix410') == -1)) {
      $result += `
   <dt>Aix410 : </dt>
   <dd>${Aix410}</dd>
    `}
    if (! (`${BnFfr23136}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr23136') == -1)) {
      $result += `
   <dt>BnFfr23136 : </dt>
   <dd>${BnFfr23136}</dd>
    `}
    if (! (`${BnFr23146}` === "undefined") && ! (searchOptions.fields.indexOf('BnFr23146') == -1)) {
      $result += `
   <dt>BnFr23146 : </dt>
   <dd>${BnFr23146}</dd>
    `}
    if (! (`${BnFfr4967}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr4967') == -1)) {
      $result += `
   <dt>BnFfr4967 : </dt>
   <dd>${BnFfr4967}</dd>
    `}
    if (! (`${BnFfr5299}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr5299') == -1)) {
      $result += `
   <dt>BnFfr5299 : </dt>
   <dd>${BnFfr5299}</dd>
    `}
    if (! (`${BnFfr23145}` === "undefined") && ! (searchOptions.fields.indexOf('BnFfr23145') == -1)) {
      $result += `
   <dt>BnFfr23145 : </dt>
   <dd>${BnFfr23145}</dd>
    `}
    if (! (`${Cha514}` === "undefined") && ! (searchOptions.fields.indexOf('Cha514') == -1)) {
      $result += `
   <dt>Cha514 : </dt>
   <dd>${Cha514}</dd>
    `}
    if (! (`${Vat966}` === "undefined") && ! (searchOptions.fields.indexOf('Vat966') == -1)) {
      $result += `
   <dt>Vat966 : </dt>
   <dd>${Vat966}</dd>
    `}
    $result += `
</dl>`
    return $result
}).join('\n')
}

if (results.length > 0) {
$app.classList.add('hasResults')
} else {
$app.classList.remove('hasResults')
}
}

const getSuggestions = (query) => {
return miniSearch.autoSuggest(query, { boost: { source_author: 3, fragment: 6} })
.filter(({ suggestion, score }, _, [first]) => score > first.score / 4)
.slice(0, 5)
}
