window.onload = function () {
  visuInit();
  exploreInit();
  dataHandler.init();
  imageHandler.init();

  changePath();
}

async function getData() {
  const response = await fetch("../../static/json/list_paints_zones.json");
  return response.json();
}

// Init tabs to show : in function of the chosen visualisation (image, diplo,
// linear) for left and right tab
function visuInit() {

  var chosenVisu = document.getElementById("visu").textContent;
  //par défaut - utiliser le tableau pour ensuite chopper la bonne visu pour la variable riht qui affiche la description
  let visuPos = { "image": "left", "description": "right", "biblio": null }

  // Default values
  var leftVisu = "image-left";
  var rightVisu = "description-right";

  if (chosenVisu != "") {
    leftVisu = chosenVisu.split("_")[0];
    rightVisu = chosenVisu.split("_")[1];
  } else {
    document.getElementById("visu").textContent = leftVisu + "_" + rightVisu;
  }

  // save position for every content type
  for (let content in visuPos) {
    if (leftVisu.includes(content)) {
      visuPos[content] = "left";
    } else if (rightVisu.includes(content)) {
      visuPos[content] = "right";
    } else {
      visuPos[content] = null;
    }
  }

  var left = document.getElementById(leftVisu);
  var right = document.getElementById(rightVisu);
  // boutons nav-tab
  var tabLeft = document.getElementById(leftVisu + '-tab');
  var tabRight = document.getElementById(rightVisu + '-tab');

  var toActive = [tabLeft, tabRight, left, right];
  var toShow = [left, right];

  for (var tab of toActive) {
    tab.classList.add('active');
  }
  for (var tab of toShow) {
    tab.classList.add('show');
  }

  // TODO peut etre pas besoin d'écrire ça ici parce que normalement que l'initatilisation non ?
  // cas d'il y a une page choisie vs cas où on prend la première page
  if (visuPos["description"] != null) {
    let thumbnailId = document.querySelector('.img-thumbnail.current') ? document.querySelector('.img-thumbnail.current').id : document.querySelectorAll('.img-thumbnail')[0].id;
    document.querySelector("#description-" + visuPos["description"] + "-" + thumbnailId).classList.add("current-page-text", "show");
  }
}

const imageHandler = {
  currImg: "",
  imgPos: getImagePos(),
  viewer: Array(),
  options: {
    id: "fabricjsOverlay",
    selection: false, // NE MARCHE PAS
    // to detail hovering zone
    perPixelTargetFind: true,
    targetFindTolerance: 0
  },
  overlays: [],
  initSide: function (side) {
    imageHandler.viewer[side] = new OpenSeadragon.Viewer({
      id: `openseadragon-${side}`,
      showNavigator: false,
      showRotationControl: true,
      prefixUrl: '../../static/img/libs/',
      zoomInButton: `osd-zoom-in-${side}`,
      zoomOutButton: `osd-zoom-out-${side}`,
      homeButton: `osd-home-${side}`,
      fullPageButton: `osd-full-page-${side}`,
      displayZonesButton: `osd-zones-${side}`
    });
    new OpenSeadragon.Button({
      element: OpenSeadragon.getElement(`osd-zones-${side}`),
      tooltip: 'Draw or remove zones',
      onClick: imageHandler.drawRemoveZones
    });

    let url = document.querySelectorAll('.img-thumbnail')[0].dataset.img;

    let pageNb = document.getElementById("pageNb").innerText;
    if (pageNb != "") {
      url = document.querySelector('#f' + pageNb).dataset.img;
      // display img
      imageHandler.display(side, "f" + pageNb);
    } else {
      imageHandler.display(side, document.querySelectorAll('.img-thumbnail')[0].id);
    }

    imageHandler.currImg = url;

    // initialisation de l'overlay pour y afficher les zonages
    setTimeout(() => {
      this.options.scale = dataHandler.scales[url];
      this.overlays[side] = this.viewer[side].fabricjsOverlay(this.options);
    }, "800");

  },
  init: function () {
    this.initSide("left");
    this.initSide("right");

    const thumbnails = document.querySelectorAll('.img-thumbnail');
    [].forEach.call(thumbnails, function (element) {
      element.addEventListener("click", imageHandler.change);
    });

    window.onscroll = function () {
      let body = document.querySelector('body');
      if (window.scrollY >= 300) {
        body.classList.add("scrolled");
      } else {
        body.classList.remove("scrolled");
      }
    };

    let descriptionsZones = document.querySelectorAll(".description-zone");
    for (let i = 0; i < descriptionsZones.length; i++) {
      descriptionsZones[i].addEventListener(
        "mouseup",
        function (event) {

          let pos = getImagePos();
          let currentImage = imageHandler.currImg;

          // version affiche/cache les zones (en les additionnant) au clic dans le texte
          if (!document.querySelector(".img-thumbnail.current").classList.contains("zonesActivated")) {
            // afficher les zones
            let polyIdToChange = 1;
            let removing = false
            let textTag = event.target.parentElement.parentElement;

            // récupère les id des grand-parents pour savoir quelles zones afficher/masquer
            for (let a = 0; a < textTag.classList.length; a++) {
              zoneId = textTag.classList[a];
              for (let z = 0; z < dataHandler.polygonsAreas[currentImage].length; z++) {
                const polyId = dataHandler.polygonsAreas[currentImage][z].id;
                if (zoneId == polyId) {
                  polyIdToChange = polyId;
                  break;
                }
              }

              // si déjà affiché et qu'on veut masquer
              if (imageHandler.overlays[pos].fabricCanvas()._objects.length > 0) {
                let drawnPolys = imageHandler.overlays[pos].fabricCanvas()._objects;

                drawnPolys.forEach(drawnPoly => {
                  if (drawnPoly.id == polyIdToChange) {
                    removing = true;

                    let polyIndex = imageHandler.overlays[pos].fabricCanvas()._objects.indexOf(drawnPoly);
                    let polyToRemove = imageHandler.overlays[pos].fabricCanvas().item(polyIndex);

                    imageHandler.overlays[pos].fabricCanvas().remove(polyToRemove);
                    imageHandler.overlays[pos].fabricCanvas().renderAll();
                  }
                });

              }
              // si on veut afficher
              if (!removing) {
                // dessine les zones
                let polyToAdd = new fabric.Polygon(dataHandler.polygons[currentImage][polyIdToChange].points, dataHandler.polygons[currentImage][polyIdToChange].infos);

                imageHandler.overlays[pos].fabricCanvas().add(polyToAdd);

                let nbPoly = imageHandler.overlays[pos].fabricCanvas()._objects.length - 1;
                imageHandler.overlays[pos].fabricCanvas().item(nbPoly).set('fill', 'transparent');
                imageHandler.overlays[pos].fabricCanvas().item(nbPoly).set('dirty', 'true')
                imageHandler.overlays[pos].fabricCanvas().requestRenderAll();
                imageHandler.overlays[pos].fabricCanvas().item(nbPoly).set('dirty', 'false')
              }
            }
            removing ? textTag.classList.remove("corresp") : textTag.classList.add("corresp");
          }

          // version colorie les zones sans les additionner
          else {
            corresps = document.querySelectorAll(".corresp")

            // masquer les descriptions sélectionnées précédement
            let item = 1;
            for (let y = 0; y < corresps.length; y++) {
              corresps[y].classList.remove("corresp");

              // récupère les id des grand-parents pour savoir quelles zones masquer
              for (let a = 0; a < corresps[y].classList.length; a++) {
                zoneId = corresps[y].classList[a];
                for (let z = 0; z < dataHandler.polygonsAreas[currentImage].length; z++) {
                  const polyId = dataHandler.polygonsAreas[currentImage][z].id;
                  let polyNumber = z;
                  if (zoneId == polyId) {
                    item = polyNumber;
                    break;
                  }
                }
                imageHandler.overlays[pos].fabricCanvas().item(item).set('fill', 'transparent');
                imageHandler.overlays[pos].fabricCanvas().item(item).set('dirty', 'true')
                imageHandler.overlays[pos].fabricCanvas().requestRenderAll();
                imageHandler.overlays[pos].fabricCanvas().item(item).set('dirty', 'false')
              }
            }

            // afficher les zones
            item = 1;
            // récupère les id des grand-parents pour savoir quelles zones afficher
            parentClasses = event.target.parentElement.parentElement.classList;
            for (let a = 0; a < parentClasses.length; a++) {
              zoneId = parentClasses[a];
              for (let z = 0; z < dataHandler.polygonsAreas[currentImage].length; z++) {
                const polyId = dataHandler.polygonsAreas[currentImage][z].id;
                let polyNumber = z;
                if (zoneId == polyId) {
                  item = polyNumber;
                  break;
                }
              }
              imageHandler.overlays[pos].fabricCanvas().item(item).set('fill', '#DEC9BF');
              imageHandler.overlays[pos].fabricCanvas().item(item).set('dirty', 'true')
              imageHandler.overlays[pos].fabricCanvas().requestRenderAll();
              imageHandler.overlays[pos].fabricCanvas().item(item).set('dirty', 'false')
            }
            parentClasses.add("corresp");
          }

        },
        false,
      );
    }

  },
  drawRemoveZones: function () {
    let image = document.querySelector(".img-thumbnail.current")
    let url = image.dataset.img;

    clearAll();
    if (image.classList.contains("zonesActivated")) {
      image.classList.remove("zonesActivated")
    } else {
      image.classList.add("zonesActivated")
      displayAllFigures(url);
    }

    // masquer les descriptions sélectionnées précédement
    corresps = document.querySelectorAll(".corresp")
    for (let corresp of corresps) {
      corresp.classList.remove('corresp');
    }
  },
  change: function (evt) {
    let previous;
    let thumbnail = evt.currentTarget;
    let pos = getImagePos();
    
    if (previous = document.querySelector('.img-thumbnail.current')) {
      previous.classList.remove('current');
    }
    thumbnail.classList.add('current');
    
    activeTabs = document.querySelectorAll(".nav-link.active");
    for (let activeTab of activeTabs) {
      if (activeTab.id.includes("image")) {
        pos = activeTab.id.split("-")[1];
      }
    }
    
    clearAll();
    imageHandler.display(pos, thumbnail.id);
    
    imageHandler.currImg = thumbnail.dataset.img;
    changePath();

  },

  display: function (imgPos, imageRef) {
    let url = document.querySelector("#" + imageRef).dataset.img;
    
    if (url) {
      imageHandler.options.scale = dataHandler.scales[url];
      imageHandler.overlays[imgPos] = imageHandler.viewer[imgPos].fabricjsOverlay(imageHandler.options);
      imageHandler.viewer[imgPos].open({
        type: 'image',
        url: url
      });
    }
    let textPos = imgPos == "left" ? "right" : "left";
    this.displayText(textPos, imageRef);
  },
  hideText: function () {
    let pageTextDivs = document.querySelectorAll('.current-page-text');
    for (var pageTextDiv of pageTextDivs) {
      pageTextDiv.classList.remove('current-page-text');
    }
  },
  displayText: function (textPos, imageRef) {
    this.hideText();
    document.querySelector(`#description-${textPos}-${imageRef}`).classList.add('current-page-text');
  }
}

const displayFigures = function (side, url) {
  imageHandler.overlays[side].fabricCanvas().clear();

  // draw polygons zones in descending orders of areas to avoid overlaps
  for (let i = 0; i < dataHandler.polygonsAreas[url].length; i++) {
    let polyId = dataHandler.polygonsAreas[url][i].id;
    let poly = new fabric.Polygon(dataHandler.polygons[url][polyId].points, dataHandler.polygons[url][polyId].infos);

    imageHandler.overlays[side].fabricCanvas().add(poly);
  }

  // highlight clicked zone description
  imageHandler.overlays[side].fabricCanvas().on('mouse:up', function (e) {
    let previousRefs = document.querySelectorAll(".corresp");
    for (var previousRef of previousRefs) {
      previousRef.classList.remove('corresp');
    }
    if (e.target) {
      let refs = document.querySelectorAll("." + e.target["id"]);
      for (var ref of refs) {
        if (!ref.classList.contains("corresp")) {
          ref.classList.add('corresp');
          ref.scrollIntoView({ block: "center" });
        }
      }
    }
  });

  // color overflown zones
  imageHandler.overlays[side].fabricCanvas().on('mouse:over', function (e) {
    if (e.target) {
      e.target.set('fill', '#DEC9BF')
      e.target.set('opacity', '0.5')
      // redraw only dirty zones
      e.target.set('dirty', 'true')
      imageHandler.overlays[side].fabricCanvas().requestRenderAll();
      e.target.set('dirty', 'false')

      // display zones notes
      document.querySelector("#zones-infos").innerText = document.querySelector("#" + e.target["id"] + "-note").innerText;
      document.querySelector("#zones-infos").style.display = "block";
    }
  });

  // uncolor preceding overflown zones
  imageHandler.overlays[side].fabricCanvas().on('mouse:out', function (e) {
    if (e.target) {
      e.target.set('fill', 'transparent')
      // redraw only dirty zones
      e.target.set('dirty', 'true')
      imageHandler.overlays[side].fabricCanvas().requestRenderAll();
      e.target.set('dirty', 'false')

      // display zones notes
      document.querySelector("#zones-infos").innerText = "";
      document.querySelector("#zones-infos").style.display = "none";
    }
  });
}

// display described zones on images
const displayAllFigures = function (url) {
  clearAll();
  imageHandler.overlays["left"]._scale = dataHandler.scales[url];
  imageHandler.overlays["right"]._scale = dataHandler.scales[url];
  displayFigures("left", url);
  displayFigures("right", url);
}




// get current path, chapter, page and visu informations to change url
function changePath() {
  var ancientPath = window.location.href;
  var newPath = window.location.pathname.split("/").slice(0, 2).join("/");
  var visuLeft = document.getElementById("nav-tab-left").getElementsByClassName("nav-link active")[0].getAttribute("aria-controls");
  var visuRight = document.getElementById("nav-tab-right").getElementsByClassName("nav-link active")[0].getAttribute("aria-controls");
  var visu = visuLeft + "_" + visuRight;

  document.getElementById("visu").textContent = visu;
  var pageNum = document.getElementsByClassName("current-page-text")[0].getAttribute("data-img-id");
  var page = pageNum.replace("f", "p");

  newPath += "/" + page + "/" + visu;

  let stateObj = {
    id: "url"
  };
  window.history.replaceState(stateObj, "", newPath);

  // active good thumbnail
  document.querySelectorAll(".img-thumbnail.current").forEach(element => {
    element.classList.remove("current");
  });
  document.querySelector(`#${pageNum}`).classList.add("current");

  // display description
  if (visuLeft.includes("description")) {
    document.querySelector(`#${visuLeft}-${pageNum}`).classList.add("current-page-text", "show");
  } if (visuRight.includes("description")) {
    document.querySelector(`#${visuRight}-${pageNum}`).classList.add("current-page-text", "show");
  }
  // display image
  if (visuLeft.includes("description")) {
    document.querySelector(`#${visuLeft}-${pageNum}`).classList.add("current-page-text", "show");
  } else if (visuLeft.includes("image")) {
    imageHandler.display("left", pageNum);
  }
  if (visuRight.includes("description")) {
    document.querySelector(`#${visuRight}-${pageNum}`).classList.add("current-page-text", "show");
  } else if (visuRight.includes("image")) {
    imageHandler.display("right", pageNum);
  }
}

function calculateArea(coords) {
  let area = 0;
  for (let i = 0; i < coords.length; i++) {
    const [x1, y1] = coords[i];
    const [x2, y2] = coords[(i + 1) % coords.length];
    area += x1 * y2 - x2 * y1
  }
  return Math.abs(area) / 2;
}

function getImagePos() {
  let pos = "left";
  document.querySelectorAll(".tab-pane.active").forEach(tab => {
    if (tab.id.includes("image")) {
      pos = tab.id.split("-")[1]
    }
  });
  return pos;
}

function clearAll() {
  imageHandler.overlays["left"].fabricCanvas().clear();
  imageHandler.overlays["right"].fabricCanvas().clear();
}

//display note near cursor
window.onmousemove = function (e) {
  if (document.querySelector("#zones-infos").style.display == "block") {
    let mouseX = e.clientX;
    let mouseY = e.clientY;
    let noteX = document.querySelector("#zones-infos").getBoundingClientRect().width;
    let noteY = document.querySelector("#zones-infos").getBoundingClientRect().height;

    document.querySelector("#zones-infos").style.top = (mouseY - noteY) + 'px';
    document.querySelector("#zones-infos").style.left = (mouseX - noteX) + 'px';
  }
};

const dataHandler = {
  scales: Object(),
  polygons: Object(),
  polygonsAreas: Array(),
  init: function () {
    getData().then((data) => {
      let jsonMention = data[0];

      let polygonsPoints = Object();
      dataHandler.polygonsAreas = [];

      for (let currentImage in jsonMention) {

        dataHandler.polygons[currentImage] = {}
        dataHandler.polygonsAreas[currentImage] = []

        // gestion de l'échelle des zones//images
        let scale = jsonMention[currentImage][0]
        dataHandler.scales[currentImage] = scale;

        let infosFigure = jsonMention[currentImage][1]

        // get polygons points and their areas
        for (let figure in infosFigure) {
          let points = infosFigure[figure];

          let newpoints = [];
          for (let index = 0; index < points.length; index++) {
            newpoints.push([points[index]["x"], points[index]["y"]]);
          }
          polygonsPoints[figure] = points;
          dataHandler.polygonsAreas[currentImage].push({ "id": figure, "area": calculateArea(newpoints) });
        }

        // sort polygons by area
        dataHandler.polygonsAreas[currentImage].sort(function (a, b) {
          return a.area - b.area;
        });
        dataHandler.polygonsAreas[currentImage].reverse();

        // draw polygons zones in descending orders of areas to avoid overlaps
        for (let i = 0; i < dataHandler.polygonsAreas[currentImage].length; i++) {

          let polyId = dataHandler.polygonsAreas[currentImage][i].id;
          dataHandler.polygons[currentImage][polyId] = { "points" : [], "infos" : {}};

          // add poly infos to create zone
          dataHandler.polygons[currentImage][polyId]["points"] = polygonsPoints[polyId];
          dataHandler.polygons[currentImage][polyId]["infos"] = {
            id: polyId,
            stroke: '#B22222',
            strokeWidth: 3,
            fill: 'transparent',
            // make it impossible to drag or resize zone
            lockRotation: true,
            lockScalingX: true,
            lockScalingY: true,
            lockMovementX: true,
            lockMovementY: true,
            // to detail hovering zone
            perPixelTargetFind: true,
            // remove selection lines
            hasBorders: false,
            hasControls: false,
            // remove draggability indicator
            setControlsVisibility: ({ mtr: false }),
            dirty: false,
            hoverCursor: 'pointer'
          };
        }
      }
    })
  }

}
