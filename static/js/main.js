// Stop fix navbar + hide footer
function exploreInit() {
  var navbar = document.getElementsByClassName("navbar")[0];
  navbar.classList.remove("sticky-top");
  var footer = document.getElementsByClassName("footer")[0];
  footer.style.display = "none";
}

window.onload = function() {
  var buttons = document.querySelectorAll(".btn-icons");
  buttons.forEach(element => {
    element.addEventListener("click", () => {
      var icon = element.querySelector("i");
      if (icon.classList.contains('fa-angle-down')) {
        icon.classList.remove('fa-angle-down');
        icon.classList.add('fa-angle-up');
        element.querySelector("a").classList.add("selected");
      } else {
        icon.classList.remove('fa-angle-up');
        icon.classList.add('fa-angle-down');
        element.querySelector("a").classList.remove("selected");
      }
    });
  });
}