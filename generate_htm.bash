#!/bin/bash

log="generate_htm.log"
echo "Log "`date` > $log


echo "# Création des sorties html pour les pages éditoriales" | tee -a $log
xsl="data/xslt/tei2editorial.xsl"
echo "  XSL: "$xsl

inputDir="data/xml/CreNum_accueil.xml data/xml/CreNum_bibliographie.xml data/xml/CreNum_carriere_Cretin.xml data/xml/CreNum_equipe.xml data/xml/CreNum_presentation_de_la_CF.xml data/xml/CreNum_principes_editoriaux.xml"
outputDir="templates/htm/"

# Parcours des fichiers à transformer
for inputFile in $inputDir
do
  # Création des chemins/noms des fichiers de sorties
  pageName=$(basename $inputFile ".xml")
  fileName=$(echo $pageName | cut -d '_' -f 2-)
  outputFile=$outputDir$fileName".htm"
  # Transformation
  saxonb-xslt -o $outputFile $inputFile $xsl 2>>$log
  echo "  - Input:  "$inputFile
  echo "  - Output: "$outputFile
  echo
done

xsl="data/xslt/tei_msDesc2editorial.xsl"
echo "  XSL: "$xsl

input="data/xml/CreNum_header_manuscrits.xml"
output="templates/htm/manuscrits.htm"
saxonb-xslt -w0 $input $xsl 1>$output 2>>$log
echo "  - Input:  "$input
echo "  - Output: "$output
echo

# Ne fonctionne pas car saxonb ne gère pas les XPointer... Argggg
echo "# Création de l'index des personnes" | tee -a generate_htm.log
echo "\e[31m  # Utiliser Oxygen"
xsl="data/xslt/teiAll2indexPersName.xsl"
echo "  XSL: "$xsl
input="data/xml/CreNum_all.xml"
output="templates/htm/index_person.htm"
echo "\e[37msaxonb-xslt -w0 -ext:on -xi $input $xsl 1>$output 2>>$log\e[31m"
echo "  - Input:  "$input
echo "  - Output: "$output
echo "\e[0m"
##saxonb-xslt -w0 -ext:on -xi data/xml/CreNum_all.xml data/xslt/teiAll2indexPersName.xsl >templates/htm/index_person.htm

echo "# Création d'une version restructurée par chapitre" | tee -a generate_htm.log
echo "  # Utiliser Oxygen"
xsl="data/xslt/tei2chapter.xsl"
echo "\e[31m  XSL: "$xsl
#
input="data/xml/CreNum_all.xml"
echo "\e[37msaxonb-xslt -w0 $input $xsl 2>>$log\e[31m"
echo "  - Input:  "$input
echo "\e[0m"
echo

echo "# Création de la mise en regard des miniatures et de leur description" | tee -a $log
input="data/xml/CreNum_facsimiles.xml"
output="templates/htm/paints.htm"
xsl="data/xslt/tei2paints.xsl"
saxonb-xslt -w0 $input $xsl 1>$output 2>>$log
echo "  - Input:  "$input
echo "  - Output: "$output

echo "# Récupération des informations pour reconstituer la généalogie" | tee -a $log

echo "  # Liste des personnes" | tee -a $log
input="data/xml/CreNum_header_liste_personnes.xml"
output="static/json/list_persons.json"
xsl="data/xslt/list_persons.xsl"
saxonb-xslt -w0 $input $xsl 1>$output 2>>$log
echo "  - Input:  "$input
echo "  - Output: "$output

echo "  # Liste des relations" | tee -a $log
input="data/xml/CreNum_header_liste_personnes.xml"
output="static/json/list_relations.json"
xsl="data/xslt/list_relations.xsl"
saxonb-xslt -w0 $input $xsl 1>$output 2>>$log
echo "  - Input:  "$input
echo "  - Output: "$output
echo

echo "# Récupération des informations pour la frise chronologique" | tee -a $log

echo "  # Liste des règnes" | tee -a $log
input="data/xml/CreNum_header_liste_personnes.xml"
output="static/json/list_reigns.json"
xsl="data/xslt/list_reigns.xsl"
saxonb-xslt -w0 $input $xsl 1>$output 2>>$log
echo "  - Input:  "$input
echo "  - Output: "$output
echo

echo "See generate_htm.log for logs"
