from flask import Flask
from flask import redirect
from flask import render_template
from flask import url_for

import os

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html.j2')

@app.route('/book<bookNb>')
@app.route('/book<bookNb>/<chapName>')
@app.route('/book<bookNb>/<chapName>/<visu>')
@app.route('/book<bookNb>/<chapName>/p<pageNb>')
@app.route('/book<bookNb>/<chapName>/p<pageNb>/<visu>')
def page(bookNb="1", chapName="", pageNb="", visu=""):
    chapUrl = "htm/"+bookNb+"/"
    if chapName == "":
        chapters = os.listdir("templates/"+chapUrl)
        chapName = sorted(chapters)[0]
        for chapter in chapters:
            if chapter.startswith("1_"):
                chapName = chapter
                break
        chapUrl += chapName
    else:
        chapUrl += chapName + ".htm.j2"
    chapName = chapName.split(".")[0]
    try:
        return render_template('book'+bookNb+'.html.j2', chap=chapUrl, chapName=chapName, pageNb=pageNb, visu=visu)
    except Exception:
        return render_template('index.html.j2')

@app.route('/<paintsPage>')
@app.route('/<paintsPage>/<visu>')
@app.route('/<paintsPage>/p<pageNb>')
@app.route('/<paintsPage>/p<pageNb>/<visu>')
def facsimile(paintsPage="paints1", pageNb="", visu=""):
    paintsUrl = "htm/paints/"+paintsPage+".htm.j2"
    try:
        return render_template(paintsPage+'.html.j2', paints=paintsUrl, pageNb=pageNb, visu=visu)
    except Exception:
        return render_template('index.html.j2')

@app.route('/<vueName>')
def vue(vueName=None):
    try:
        return render_template(vueName+'.html.j2')
    except Exception:
        return render_template('index.html.j2')


if __name__ == '__main__':
    #changer de port : (par défaut Flask utilise le 5000)
    #app.run(host="127.0.0.1",port=5005,debug=True)
    app.run(debug=True)
