# CreNum
Code de l'édition numérique de la Chronique française de Guillaume Cretin

## Installation

**Pré-requis**

Python3 et pip doivent être installés sur votre machine.

**Déploiement**

`git clone git@gricad-gitlab.univ-grenoble-alpes.fr:elan/crenum.git`

`cd crenum`

`python3 -m pip install -r requirements.txt`, si vous rencontrez l'erreur *Free/Libre Open Source Software Binaries of VS Code*, alors passez par : `sudo apt install python3-flask`

**Pour une utilisation ponctuelle**

Placez-vous dans le dossier `crenum-v2` et lancer la commande `python3 crenum.py`.

**En production ou pour une utilisation régulière**

*Créer un service permet de ne pas avoir à relancer le site à chaque redémarrage de l'ordinateur.*

Copier le fichier `crenum.service.dist` dans `crenum.service` et modifier dans ce dernier les éléments `<user>` et `<pathToRep>`, puis déplacez le dans le dossier `/lib/systemd/system/`.

```
[Unit]
Description=Flask web server
After=network.target

[Install]
WantedBy=multi-user.target

[Service]
User=<user>
WorkingDirectory=<pathToRep>/crenum
ExecStart=/usr/bin/python3 <pathToRep>/crenum/crenum.py
TimeoutSec=600
Restart=always
```

Lancez ensuite les deux commandes suivantes pour permettre à l'application de se relancer d'elle-même à chaque boot du serveur.
```
sudo systemctl enable crenum.service
sudo systemctl start crenum.service
```
Vous pouvez vérifier l'état de l'application à l'aide de `sudo systemctl status crenum.service`.

Si vous re-modifiez ce fichier ultérieurement, pensez bien à stopper puis relancer le service.

Pour configurer le nom de domaine, modifiez le fichier `crenum.conf` (ligne `ServerName`) et copiez-le dans le dossier `/etc/apache2/sites-available/`.
```
<VirtualHost *:80>
ServerName crenum
ProxyPreserveHost On
ProxyPass / http://localhost:5000/
ProxyPassReverse / http://localhost:5000/
Timeout 2400
ProxyTimeout 2400
</VirtualHost>
```

Si vous re-modifiez ce fichier ultérieurement, pensez bien à le désactiver puis le ré-activer.
```
sudo a2dissite crenum.conf
sudo a2ensite crenum.conf
```
Entre chacune de ces commandes il vous sera sûrement demandé de relancer apache : `sudo systemctl reload apache2` (ou `sudo service apache2 restart`).

Ajoutez une ligne au fichiers `/etc/hosts` ?

## Mise à jour des données

**Pré-requis : installer saxon**
- `apt install default-jre`
- `apt install libsaxonb-java`

Mettre à jour les fichiers sources (XML-TEI) dans le dossier crenum-v2/data/xml puis effectuer les transformations XSL adéquates. Un script bash regroupe les lignes de commande adéquates qui utilisent le transformateur *saxonb-xslt*
`sh generate_htm.bash`

## Licence sur le code
GNU GENERAL PUBLIC LICENSE V3 (voir le [Guide rapide de la GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html))

## Auteurs du code
Voir fichier `AUTHORS`

## Droits image du projet
Source gallica.bnf.fr / Bibliothèque nationale de France. Département des Manuscrits. Français 2820.
